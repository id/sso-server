package login

import (
	"net/http"
	"strings"

	"github.com/gorilla/securecookie"
)

var defaultCookieSecure = true

// A sessionStore is a simpler version of gorilla sessions.Store,
// without the global session registry, serializing specific types
// rather than generic interfaces. Each sessionStore manages a single
// session cookie.
type sessionStore struct {
	cookieName string
	cookiePath string
	sameSite   http.SameSite

	sc *securecookie.SecureCookie
}

func newSessionStore(name, path string, authKey, encKey []byte, maxAge int, sameSite http.SameSite) *sessionStore {
	sc := securecookie.New(authKey, encKey)

	// Set the securecookie time-to-live (0 disables this check).
	sc.MaxAge(maxAge)

	// The JSON encoder generates smaller data than gob in our case.
	sc.SetSerializer(&securecookie.JSONEncoder{})
	return &sessionStore{
		cookieName: name,
		cookiePath: trimSlash(path),
		sameSite:   sameSite,
		sc:         sc,
	}
}

func (s *sessionStore) getSession(req *http.Request, obj interface{}) error {
	cookie, err := req.Cookie(s.cookieName)
	if err != nil {
		return err
	}
	return s.sc.Decode(s.cookieName, cookie.Value, obj)
}

// Set the session value. Call with nil argument to delete the session cookie.
func (s *sessionStore) setSession(w http.ResponseWriter, obj interface{}) error {
	// The session cookie will always have MaxAge==0 (delete on
	// browser exit), regardless of whether the session contains an
	// internal timestamp or not.
	cookie := &http.Cookie{
		Name:     s.cookieName,
		Path:     s.cookiePath,
		Secure:   defaultCookieSecure,
		HttpOnly: true,
		SameSite: s.sameSite,
	}
	if obj == nil {
		cookie.MaxAge = -1
	} else {
		enc, err := s.sc.Encode(s.cookieName, obj)
		if err != nil {
			return err
		}
		cookie.Value = enc
	}

	http.SetCookie(w, cookie)

	return nil
}

func trimSlash(s string) string {
	if s == "/" {
		return s
	}
	return strings.TrimRight(s, "/")
}
