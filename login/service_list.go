package login

import (
	"context"
	"net/http"
)

type AuthServiceList struct {
	// Services the user has logged in to from this session.
	Services []string `json:"s"`

	// Pointer to the session store.
	store *sessionStore
}

// AddService adds a service to the current session (if it's not
// already there).
func (s *AuthServiceList) AddService(service string) {
	for _, svc := range s.Services {
		if svc == service {
			return
		}
	}
	s.Services = append(s.Services, service)
}

func (s *AuthServiceList) Save(w http.ResponseWriter) error {
	return s.store.setSession(w, s)
}

func (s *AuthServiceList) Delete(w http.ResponseWriter) error {
	return s.store.setSession(w, nil)
}

type listCtxKey int

const serviceListCtxKey listCtxKey = 0

// GetServiceList returns the AuthServiceList object associated with
// the current session.
func GetServiceList(ctx context.Context) (*AuthServiceList, bool) {
	s, ok := ctx.Value(serviceListCtxKey).(*AuthServiceList)
	return s, ok
}

type serviceListMiddleware struct {
	store *sessionStore
}

func (m *serviceListMiddleware) Wrap(wrap http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		var serviceList AuthServiceList
		m.store.getSession(req, &serviceList) // nolint: errcheck
		serviceList.store = m.store
		req = req.WithContext(context.WithValue(
			req.Context(), serviceListCtxKey, &serviceList))
		wrap.ServeHTTP(w, req)
	})
}
