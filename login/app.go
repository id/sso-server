package login

import (
	"log"
	"net/http"
	"time"

	"git.autistici.org/id/sso-server/common"
	"git.autistici.org/id/sso-server/server/device"
	"git.autistici.org/id/sso-server/ui"
	"github.com/gorilla/csrf"
	"github.com/gorilla/securecookie"
)

var (
	// Maximum duration of a single login workflow.
	sessionTTL = 15 * time.Minute

	// Default value for auth_session_lifetime.
	defaultAuthSessionLifetimeSeconds = 12 * 3600

	// A relatively strict CSP.
	defaultCSP = "default-src 'none'; img-src 'self' data:; script-src 'self'; style-src 'self'; connect-src 'self'; frame-ancestors 'none'; base-uri 'none';"

	// Slightly looser CSP for the logout page: it needs to load remote
	// images.
	logoutCSP = "default-src 'none'; img-src *; script-src 'self'; style-src 'self'; connect-src *; frame-ancestors 'none'; base-uri 'none';"
)

type Config struct {
	ui.Config `yaml:",inline"`

	AuthService                string `yaml:"auth_service"`
	AuthSessionLifetimeSeconds int    `yaml:"auth_session_lifetime"`

	DeviceManager *device.Config `yaml:"device_manager"`

	SessionAuthKey          common.SessionAuthenticationKey `yaml:"session_auth_key"`
	SessionEncKey           common.SessionEncryptionKey     `yaml:"session_enc_key"`
	CSRFSecret              common.YAMLBytes                `yaml:"csrf_secret"`
	TrustedOrigins          []string                        `yaml:"trusted_origins"`
	DefaultSignedInRedirect string                          `yaml:"default_signed_in_redirect"`
	CookieSameSiteMode      string                          `yaml:"cookie_same_site_mode"`
}

const (
	authSessionCookieName  = "auth_s"
	svcSessionCookieName   = "svclist"
	loginSessionCookieName = "login_s"
	csrfCookieName         = "_csrf"
)

// Wrap a http.Handler with an authentication web UI backed by
// authClient. The wrapped code can then call GetAuth() to obtain the
// authenticated user's details.
func Wrap(wrap http.Handler, authClient AuthClient, config *Config, urls common.URLMap) (http.Handler, error) {
	// Sanity checks on the configuration.
	if len(config.SessionAuthKey) == 0 {
		log.Printf("Warning: session_auth_key unset, generating temporary random session secrets")
		config.SessionAuthKey = securecookie.GenerateRandomKey(64)
		config.SessionEncKey = securecookie.GenerateRandomKey(32)
	}
	if config.AuthSessionLifetimeSeconds == 0 {
		config.AuthSessionLifetimeSeconds = defaultAuthSessionLifetimeSeconds
	}

	devMgr, err := device.New(config.DeviceManager, urls.URLFor("/"))
	if err != nil {
		return nil, err
	}

	staticHandler, renderer, err := ui.Build(&config.Config, urls)
	if err != nil {
		return nil, err
	}

	return doWrap(wrap, devMgr, authClient, staticHandler, renderer, urls, config), nil
}

// Internal implementation of Wrap(), factored out for modular testing.
func doWrap(wrap http.Handler, devMgr *device.Manager, authClient AuthClient, staticHandler http.Handler, renderer *common.Renderer, urls common.URLMap, config *Config) http.Handler {
	authClient = newTracingAuthClient(
		newServiceBoundAuthClient(authClient, config.AuthService))

	// SameSite mode defaults to 'strict'.
	sameSite := http.SameSiteStrictMode
	if ssp, err := parseSameSiteMode(config.CookieSameSiteMode); err == nil {
		sameSite = ssp
	}

	// Create the cookie factories we'll use:
	// Long-term authenticated session of the login app.
	authSessionStore := newSessionStore(authSessionCookieName, urls.URLFor("/"), config.SessionAuthKey, config.SessionEncKey, 0, sameSite)
	// Long-term list of authenticated services (tracked for
	// logout purposes), same scope as authSessionStore. This is a
	// separate cookie so we can re-authenticate an expired
	// session without losing this information.
	svcSessionStore := newSessionStore(svcSessionCookieName, urls.URLFor("/"), config.SessionAuthKey, config.SessionEncKey, 0, sameSite)
	// Short-term state of the login workflow.
	loginSessionStore := newSessionStore(loginSessionCookieName, urls.URLFor("/login/"), config.SessionAuthKey, config.SessionEncKey, int(sessionTTL.Seconds()), sameSite)

	// Build the login engine, which runs the login workflow application.
	engine := &loginEngine{
		urls:              urls,
		authSessionStore:  authSessionStore,
		loginSessionStore: loginSessionStore,
		authClient:        authClient,
		renderer:          renderer,
		fallbackRedirect:  config.DefaultSignedInRedirect,
		devMgr:            devMgr,
		authTTL:           time.Duration(config.AuthSessionLifetimeSeconds) * time.Second,
	}

	// Add CSRF to the login application pages only.
	engineHandler := common.WithDynamicHeaders(engine, defaultCSP)
	if config.CSRFSecret != nil {
		engineHandler = csrf.Protect(
			config.CSRFSecret,
			csrf.SameSite(csrf.SameSiteStrictMode),
			csrf.CookieName(csrfCookieName),
			csrf.TrustedOrigins(config.TrustedOrigins),
		)(engineHandler)
	}

	// Middleware adapter for the login engine.
	mw := &loginMiddleware{
		authSessionStore:  authSessionStore,
		loginSessionStore: loginSessionStore,
		urls:              urls,
		engine:            engine,
		engineHandler:     engineHandler,
	}

	// Middleware adapter for service tracking functionality.
	lw := &serviceListMiddleware{
		store: svcSessionStore,
	}

	// Now let's build the overall http.Handler containing the
	// login application (dynamic and static parts), and an
	// authenticated fall-through to the wrapped Handler.
	mux := http.NewServeMux()
	mux.Handle(urls.URLFor("/static/"), staticHandler)
	mux.HandleFunc("/favicon.ico", func(w http.ResponseWriter, req *http.Request) {
		http.NotFound(w, req)
	})

	mux.Handle("/", lw.Wrap(mw.Wrap(wrap, renderer)))

	return mux
}
