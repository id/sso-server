package login

import (
	"context"
	"errors"
	"log"
	"net/http"
	"net/url"
	"time"

	"git.autistici.org/id/auth"
	"git.autistici.org/id/sso-server/common"
)

type Auth struct {
	// User name and other information (like group membership).
	Username string         `json:"u"`
	UserInfo *auth.UserInfo `json:"ui"`

	// Sticky session ID.
	SessionID string `json:"sid"`

	// Deadline until authentication will need to be renewed. The
	// securecookie also provides a similar expiration mechanism,
	// but we do not use it here because we want to be able to
	// detect the expiration for UX purposes.
	Deadline time.Time `json:"d"`
}

var (
	errNoAuth      = errors.New("no authentication")
	errAuthExpired = errors.New("authentication expired")
)

// Check verifies that the session is valid, returns an error otherwise.
func (s *Auth) check() error {
	if s.Username == "" {
		return errNoAuth
	}
	if time.Now().After(s.Deadline) {
		return errAuthExpired
	}
	return nil
}

type ctxKey int

const authCtxKey ctxKey = 0

// GetAuth returns the current user information, if any. Presence of an Auth
// object implies that the authentication succeeded in all contexts *except*
// the logout handler.
func GetAuth(ctx context.Context) (*Auth, bool) {
	s, ok := ctx.Value(authCtxKey).(*Auth)
	return s, ok
}

type loginMiddleware struct {
	authSessionStore  *sessionStore
	loginSessionStore *sessionStore
	urls              common.URLMap
	engine            *loginEngine
	engineHandler     http.Handler
}

func (m *loginMiddleware) Wrap(wrap http.Handler, renderer *common.Renderer) http.Handler {
	mux := new(http.ServeMux)
	logoutHandler := newLogoutHandler(m.engine.authClient, renderer)

	// Forward /login/ to the login workflow. Handle the case
	// where the user is already authenticated by short-circuiting
	// the redirect to the 'r' parameter: this avoids restarting
	// the login workflow if you're already logged in.
	mux.HandleFunc(m.urls.URLFor("/login/"), func(w http.ResponseWriter, req *http.Request) {
		var auth Auth
		if m.authSessionStore.getSession(req, &auth) == nil && auth.check() == nil {
			redirURL := req.FormValue(nextURLParamName)
			http.Redirect(w, req, redirURL, http.StatusFound)
			return
		}
		m.engineHandler.ServeHTTP(w, req)
	})

	mux.HandleFunc(m.urls.URLFor("/logout"), func(w http.ResponseWriter, req *http.Request) {
		var auth Auth
		if m.authSessionStore.getSession(req, &auth) == nil {
			m.authSessionStore.setSession(w, nil) // nolint: errcheck
		}
		serveWithAuth(logoutHandler, w, req, &auth)
	})

	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		var auth Auth
		var flashErr string
		if err := m.authSessionStore.getSession(req, &auth); err == nil {
			err = auth.check()
			if err == nil {
				// Authentication succeeded.
				serveWithAuth(wrap, w, req, &auth)
				return
			} else if errors.Is(err, errAuthExpired) {
				flashErr = err.Error()
			} else {
				log.Printf("middleware: unexpected error: %v", err)
			}
		}

		// Initialize a new loginState.
		if err := m.engine.initSessionWithMessage(w, req, flashErr); err != nil {
			http.Error(w, "Internal Error", http.StatusInternalServerError)
			return
		}

		// Redirect to login workflow.
		values := make(url.Values)
		values.Set(nextURLParamName, req.URL.String())
		http.Redirect(w, req, m.urls.URLFor("/login/?"+values.Encode()), http.StatusFound)
	})

	return mux
}

// Invoke an http.Handler with the Auth object into the request context.
func serveWithAuth(h http.Handler, w http.ResponseWriter, req *http.Request, auth *Auth) {
	h.ServeHTTP(w, req.WithContext(context.WithValue(
		req.Context(), authCtxKey, auth)))
}
