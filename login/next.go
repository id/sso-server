package login

import (
	"bytes"
	"html/template"
	"net/http"
	"net/url"
)

// Name of the HTTP request parameter that we're using to track the
// "next URL", where the user should be redirected to after a
// successful login.
const nextURLParamName = "r"

// Use a tiny html.Template to generate the hidden field, to ensure
// that the parameter value (user-provided) is escaped correctly.
var (
	nextURLFieldStr = `<input type="hidden" name="{{.Name}}" value="{{.Value}}">`
	nextURLFieldTpl = template.Must(template.New("hidden").Parse(nextURLFieldStr))
)

// Make the next URL a type, so we can use its methods in templates.
type nextURL string

func nextURLFromRequest(req *http.Request) nextURL {
	return nextURL(req.FormValue(nextURLParamName))
}

// Generate the hidden <input> field.
func (next nextURL) InputField() template.HTML {
	var buf bytes.Buffer
	// nolint: errcheck
	nextURLFieldTpl.Execute(&buf, struct {
		Name  string
		Value string
	}{
		Name:  nextURLParamName,
		Value: string(next),
	})

	// The above already produces escaped HTML, we can just mark
	// it as safe markup for the template engine.
	// nolint: gosec
	return template.HTML(buf.String())
}

// Augment a URI with the "next URL" parameter.
func (next nextURL) AddParam(uri string) string {
	values := make(url.Values)
	values.Set(nextURLParamName, string(next))
	return uri + "?" + values.Encode()
}
