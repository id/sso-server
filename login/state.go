// Package login implements the authentication workflow, protecting
// another application by means of its Wrap() middleware.
//
// Internally, it runs a simple state machine meant to match the
// interactions with the underlying auth-server. State transitions
// happen on POST requests. Request handling is split into two stages:
// the processing stage (processing request parameters and eventually
// modifying the current state), and the rendering stage (which
// renders content to the user).
//
// - we start from the BEGIN state, where the user is asked for
// username and password.
//
// - we make the first AuthRequest, which has two possible non-error
// return values ("ok" and "need 2fa"), resulting in the OK or 2FA
// states.
//
// - in the 2FA state, we present the user with a request for the
// second authentication factor. We make a second AuthRequest that
// includes second factor information, resulting in the OK state if
// successful.
//
// States are tied to specific URLs because we want to make states
// visible to the browser, and possibly give users the option of
// hitting the back button -- though doing so will likely result in
// being reset to the BEGIN state (but it makes it easier to have
// multiple endpoints for the 2FA state).
//
// The login state machine is stored in a short-lived session cookie,
// which is global browser state, but the original_url parameter must
// instead be tracked per-window, so it must be brought along the
// request flow as a form parameter.
//
package login

import (
	"context"
	"errors"
	"log"
	"net/http"
	"strings"
	"time"

	"git.autistici.org/id/auth"
	"git.autistici.org/id/sso-server/common"
	"git.autistici.org/id/sso-server/server/device"
	"git.autistici.org/id/usermetadb"
	"github.com/go-webauthn/webauthn/protocol"
)

// AuthClient is a wrapper interface for an id/auth.Client that adds
// support for a Logout event. This allows injection of state-aware
// components that can trigger on both successful authentication and
// logout to maintain external session-scoped state.
type AuthClient interface {
	Authenticate(context.Context, *auth.Request) (*auth.Response, error)
	Logout(context.Context, string, *auth.UserInfo) error
}

const (
	StateBEGIN = iota
	State2FA_OTP
	State2FA_U2F
	StateOK
)

// State enum for the login state machine.
type State int

func (s State) String() string {
	switch s {
	case StateBEGIN:
		return "BEGIN"
	case State2FA_OTP:
		return "2FA_OTP"
	case State2FA_U2F:
		return "2FA_U2F"
	case StateOK:
		return "OK"
	default:
		return "UNKNOWN"
	}
}

var (
	loginURLStateMap = map[string]State{
		"/login/":    StateBEGIN,
		"/login/u2f": State2FA_U2F,
		"/login/otp": State2FA_OTP,
	}

	loginStateURLMap = map[State]string{
		StateBEGIN:   "/login/",
		State2FA_U2F: "/login/u2f",
		State2FA_OTP: "/login/otp",
	}

	loginStateTemplate = map[State]string{
		StateBEGIN:   "login_password.html",
		State2FA_U2F: "login_u2f.html",
		State2FA_OTP: "login_otp.html",
	}
)

type loginState struct {
	State State `json:"s"`

	// Flash messaegs.
	Error string `json:"e"`

	// Saved data for building a full AuthRequest.
	Username     string         `json:"u"`
	Password     string         `json:"p"`
	AuthResponse *auth.Response `json:"ar"`

	SessionID string `json:"sid"`

	store *sessionStore
}

func (l *loginState) Save(w http.ResponseWriter) error {
	return l.store.setSession(w, l)
}

func (l *loginState) Delete(w http.ResponseWriter) error {
	return l.store.setSession(w, nil)
}

func (l *loginState) authRequestFromRequest(req *http.Request, deviceInfo *usermetadb.DeviceInfo) (*auth.Request, error) {
	switch l.State {
	case StateBEGIN:
		l.Username = normalizeUsername(req.FormValue("username"))
		l.Password = req.FormValue("password")
		return &auth.Request{
			Username:   l.Username,
			Password:   []byte(l.Password),
			DeviceInfo: deviceInfo,
		}, nil

	case State2FA_OTP:
		return &auth.Request{
			Username:   l.Username,
			Password:   []byte(l.Password),
			DeviceInfo: deviceInfo,
			OTP:        req.FormValue("otp"),
		}, nil

	case State2FA_U2F:
		webauthnResp, err := protocol.ParseCredentialRequestResponseBody(
			strings.NewReader(req.FormValue("webauthn")))
		if err != nil {
			return nil, err
		}
		return &auth.Request{
			Username:         l.Username,
			Password:         []byte(l.Password),
			DeviceInfo:       deviceInfo,
			WebAuthnSession:  l.AuthResponse.WebAuthnSession,
			WebAuthnResponse: webauthnResp,
		}, nil

	default:
		return nil, errors.New("unknown state")
	}
}

func (l *loginState) process(req *http.Request, ac AuthClient, deviceInfo *usermetadb.DeviceInfo) error {
	authReq, err := l.authRequestFromRequest(req, deviceInfo)
	if err != nil {
		return err
	}

	authResp, err := ac.Authenticate(
		withSessionID(req.Context(), l.SessionID), authReq)
	if err != nil {
		return err
	}

	log.Printf(
		"authentication request: %s,sid=%s -> %s",
		authRequestDebugString(authReq),
		l.SessionID,
		authResponseDebugString(authResp),
	)

	l.AuthResponse = authResp
	switch authResp.Status {
	case auth.StatusOK:
		l.State = StateOK
	case auth.StatusInsufficientCredentials:
		if len(authResp.TFAMethods) < 1 {
			return errors.New("inconsistent 2FA response from auth-server, nil tfa_methods but 2fa requested")
		}
		if authResp.TFAMethods[0] == auth.TFAMethodU2F {
			l.State = State2FA_U2F
		} else {
			l.State = State2FA_OTP
		}
	default:
		l.State = StateBEGIN
		l.Error = "Authentication failed"
		l.AuthResponse = nil
	}
	return nil
}

func (l *loginState) switchState(req *http.Request, newState State) error {
	if l.State != State2FA_U2F && l.State != State2FA_OTP {
		return errors.New("invalid source state")
	}

	var method auth.TFAMethod
	switch newState {
	case State2FA_OTP:
		method = auth.TFAMethodOTP
	case State2FA_U2F:
		method = auth.TFAMethodU2F
	default:
		return errors.New("invalid target state")
	}

	// Verify that the auth-server supports this method.
	var found bool
	for _, m := range l.AuthResponse.TFAMethods {
		if m == method {
			found = true
			break
		}
	}
	if !found {
		return errors.New("unavailable 2FA method")
	}

	l.State = newState

	return nil
}

func (l *loginState) render(renderer *common.Renderer, urls common.URLMap, w http.ResponseWriter, req *http.Request, urlPath, fallbackRedirect string) {
	next := nextURLFromRequest(req)
	if next == "" {
		next = nextURL(fallbackRedirect)
	}

	// If we have reached the final state, send the user back to
	// the originally requested URL.
	if l.State == StateOK {
		l.Delete(w) // nolint: errcheck
		http.Redirect(w, req, string(next), http.StatusFound)
		return
	}

	// If the (new) state is inconsistent with the requested path, send
	// a redirect to the new page.
	if newPath := loginStateURLMap[l.State]; urlPath != newPath {
		if err := l.Save(w); err != nil {
			http.Error(w, "Internal Error", http.StatusInternalServerError)
			return
		}
		redirect(w, req, urls.URLFor(newPath))
		return
	}

	// Pop the error message from the session, if any.
	flashErr := l.Error
	l.Error = ""
	if err := l.Save(w); err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}

	renderer.Render(w, req, loginStateTemplate[l.State], map[string]interface{}{
		"Session": l,
		"Error":   flashErr,
		"NextURL": next,
	})
}

type loginEngine struct {
	urls              common.URLMap
	authSessionStore  *sessionStore
	loginSessionStore *sessionStore
	renderer          *common.Renderer
	authClient        AuthClient
	devMgr            *device.Manager
	fallbackRedirect  string
	authTTL           time.Duration
}

func (e *loginEngine) initSessionWithMessage(w http.ResponseWriter, req *http.Request, msg string) error {
	// Initialize a new loginState.
	state := &loginState{
		State: StateBEGIN,
		Error: msg,
		store: e.loginSessionStore,
	}
	return state.Save(w)
}

func (e *loginEngine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	var state loginState
	if err := e.loginSessionStore.getSession(req, &state); err != nil {
		state.State = StateBEGIN
	}
	state.store = e.loginSessionStore
	if state.SessionID == "" {
		state.SessionID = newSessionID()
	}

	// If we're supposed to handle this page, ensure that the
	// state and URL match, or redirect the user back to the
	// beginning of the login workflow.
	urlPath := e.urls.Resolve(req.URL.Path)
	expectedState, ok := loginURLStateMap[urlPath]
	if !ok {
		http.NotFound(w, req)
		return
	}
	if expectedState != state.State {
		// The user has switched state unexpectedly,
		// only one such transition is allowed, to
		// switch between different 2FA methods.
		if err := state.switchState(req, expectedState); err != nil {
			log.Printf("warning: mismatched state %s for url %s", state.State, req.URL.Path)
			if err := e.initSessionWithMessage(w, req, err.Error()); err != nil {
				goto fail
			}
			redirect(w, req, e.urls.URLFor("/login/"))
			return
		}
	}

	// Run the processing phase if this is a POST request.
	if req.Method == "POST" {
		err := state.process(req, e.authClient, e.devMgr.GetDeviceInfoFromRequest(w, req))
		if err != nil {
			log.Printf("process() error: %v", err)
			state.Error = err.Error()
		}
	}

	// If we have reached the final state, log in the user.
	if state.State == StateOK {
		// Create a new Auth session cookie, delete the
		// loginSession.
		auth := Auth{
			Username:  state.Username,
			UserInfo:  state.AuthResponse.UserInfo,
			SessionID: state.SessionID,
			Deadline:  time.Now().Add(e.authTTL),
		}
		if err := e.authSessionStore.setSession(w, &auth); err != nil {
			goto fail
		}
		log.Printf("successful login for %s", auth.Username)
	}

	state.render(e.renderer, e.urls, w, req, urlPath, e.fallbackRedirect)
	return

fail:
	http.Error(w, "Internal Error", http.StatusInternalServerError)
}

// Internal redirect that ensures we do not loose track of the 'r' parameter.
func redirect(w http.ResponseWriter, req *http.Request, uri string) {
	next := nextURLFromRequest(req)
	uri = next.AddParam(uri)
	http.Redirect(w, req, uri, http.StatusFound)
}
