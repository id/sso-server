package login

import (
	"context"
	"encoding/base32"

	"git.autistici.org/id/sso-server/common"
)

func newSessionID() string {
	// Session IDs just need to be unique per user, so 80 bits of
	// randomness are going to be plenty (also it makes for nice
	// base32 without padding).
	b := common.RandomBytes(10)
	return base32.StdEncoding.EncodeToString(b)
}

const sessCtxKey ctxKey = 1

// GetSessionID retrieves the session ID which is available during the
// AuthClient.Authenticate call.
func GetSessionID(ctx context.Context) (string, bool) {
	s, ok := ctx.Value(sessCtxKey).(string)
	return s, ok
}

func withSessionID(ctx context.Context, sessionID string) context.Context {
	return context.WithValue(ctx, sessCtxKey, sessionID)
}
