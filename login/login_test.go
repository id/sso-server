package login

import (
	"context"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"git.autistici.org/id/auth"
	"git.autistici.org/id/sso-server/common"
	"git.autistici.org/id/sso-server/server/device"
	"github.com/go-webauthn/webauthn/protocol"
	"github.com/go-webauthn/webauthn/webauthn"
	"github.com/gorilla/securecookie"
)

func init() {
	defaultCookieSecure = false
}

var fakeWebauthnSession = &webauthn.SessionData{
	Challenge: "chal",
}

func validWebAuthnRequest(req *auth.Request) bool {
	// We don't really perform a WebAuthN validation, just verify
	// that the session data is round-tripped correctly through
	// the login app.
	return req.WebAuthnSession != nil && req.WebAuthnSession.Challenge == fakeWebauthnSession.Challenge
}

type fakeAuthClient struct {
	sessionID string
}

func (c *fakeAuthClient) Authenticate(ctx context.Context, req *auth.Request) (*auth.Response, error) {
	if sid, ok := GetSessionID(ctx); ok {
		c.sessionID = sid
	}

	chal, _ := protocol.CreateChallenge()

	p := string(req.Password)
	info := &auth.UserInfo{
		Shard:  "shard1",
		Groups: []string{"users"},
	}
	switch {
	case req.Username == "testuser" && p == "password":
		return &auth.Response{Status: auth.StatusOK, UserInfo: info}, nil

	case req.Username == "testuser-otp" && p == "password" && req.OTP == "123456":
		return &auth.Response{Status: auth.StatusOK, UserInfo: info}, nil
	case req.Username == "testuser-otp" && p == "password" && req.OTP == "":
		return &auth.Response{
			Status:     auth.StatusInsufficientCredentials,
			TFAMethods: []auth.TFAMethod{auth.TFAMethodOTP},
		}, nil

	case req.Username == "testuser-webauthn" && p == "password" && validWebAuthnRequest(req):
		return &auth.Response{Status: auth.StatusOK, UserInfo: info}, nil
	case req.Username == "testuser-webauthn" && p == "password" && req.WebAuthnResponse == nil:
		return &auth.Response{
			Status:          auth.StatusInsufficientCredentials,
			TFAMethods:      []auth.TFAMethod{auth.TFAMethodU2F},
			WebAuthnSession: fakeWebauthnSession,
			WebAuthnData: &protocol.CredentialAssertion{
				Response: protocol.PublicKeyCredentialRequestOptions{
					Challenge:      chal,
					RelyingPartyID: "rp",
				},
			},
		}, nil

	case req.Username == "testuser-both" && p == "password" && req.OTP == "123456":
		return &auth.Response{Status: auth.StatusOK, UserInfo: info}, nil
	case req.Username == "testuser-both" && p == "password" && validWebAuthnRequest(req):
		return &auth.Response{Status: auth.StatusOK, UserInfo: info}, nil
	case req.Username == "testuser-both" && p == "password" && req.WebAuthnResponse == nil && req.OTP == "":
		return &auth.Response{
			Status:          auth.StatusInsufficientCredentials,
			TFAMethods:      []auth.TFAMethod{auth.TFAMethodU2F, auth.TFAMethodOTP},
			WebAuthnSession: fakeWebauthnSession,
			WebAuthnData: &protocol.CredentialAssertion{
				Response: protocol.PublicKeyCredentialRequestOptions{
					Challenge:      chal,
					RelyingPartyID: "rp",
				},
			},
		}, nil
	}

	return &auth.Response{Status: auth.StatusError}, nil
}

func (c *fakeAuthClient) Logout(ctx context.Context, username string, userinfo *auth.UserInfo) error {
	return nil
}

type logTransport struct {
	http.RoundTripper
}

func (t *logTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	resp, err := t.RoundTripper.RoundTrip(req)
	if err != nil {
		log.Printf("%s %s -> %v", req.Method, req.URL.String(), err)
	} else {
		log.Printf("%s %s -> %d", req.Method, req.URL.String(), resp.StatusCode)

		// if vv, ok := req.Header["Cookie"]; ok {
		// 	for _, v := range vv {
		// 		log.Printf("  -> Cookie: %s", v)
		// 	}
		// }
		// if vv, ok := resp.Header["Set-Cookie"]; ok {
		// 	for _, v := range vv {
		// 		log.Printf("  <- Set-Cookie: %s", v)
		// 	}
		// }
	}
	return resp, err
}

type httpClient struct {
	client  *http.Client
	baseURI string
}

func newClient(baseURI string) *httpClient {
	jar, _ := cookiejar.New(nil)
	return &httpClient{
		client: &http.Client{
			Jar:       jar,
			Transport: &logTransport{new(http.Transport)},
		},
		baseURI: baseURI,
	}
}

func formData(values map[string]string) io.Reader {
	vv := make(url.Values)
	for k, v := range values {
		vv.Set(k, v)
	}
	return strings.NewReader(vv.Encode())
}

func (c *httpClient) request(method, uri string, r io.Reader) (string, string, error) {
	req, err := http.NewRequest(method, c.baseURI+uri, r)
	if err != nil {
		return "", "", err
	}
	if r != nil {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return "", "", err
	}
	defer resp.Body.Close()
	switch resp.StatusCode {
	case 200:
		data, _ := ioutil.ReadAll(resp.Body)
		return string(data), resp.Request.URL.Path, nil
	default:
		return "", "", fmt.Errorf("HTTP status %d", resp.StatusCode)
	}
}

// Very simple templates that are testable.
func makeTestPage(title string) string {
	return fmt.Sprintf("%s\n{{.Error}}", title)
}

func isPage(data, title string) bool {
	return strings.HasPrefix(data, title+"\n")
}

func pageError(data string) string {
	return strings.SplitN(data, "\n", 2)[1]
}

func isTargetWebsite(data string) bool {
	return data == "ok"
}

// Quick verification that the above scheme works.
func TestTestPages(t *testing.T) {
	p1 := "login\n"
	if !isPage(p1, "login") {
		t.Errorf("isPage check failed")
	}
	if s := pageError(p1); s != "" {
		t.Errorf("pageError not empty: '%s'", s)
	}

	p2 := "login\nerror"
	if pageError(p2) == "" {
		t.Errorf("pageError empty")
	}
}

// Start a test server, with the login app wrapping a test application.
func startServer(t *testing.T) (*httptest.Server, *fakeAuthClient) {
	devMgr, err := device.New(&device.Config{
		AuthKey: string(securecookie.GenerateRandomKey(32)),
	}, "")
	if err != nil {
		t.Fatal(err)
	}

	ac := &fakeAuthClient{}

	tpl := template.New("")
	tpl.New("login_password.html").Parse(makeTestPage("login")) // nolint: errcheck
	tpl.New("login_u2f.html").Parse(makeTestPage("login_u2f"))  // nolint: errcheck
	tpl.New("login_otp.html").Parse(makeTestPage("login_otp"))  // nolint: errcheck

	renderer := common.NewRenderer(
		tpl,
		map[string]interface{}{},
	)

	h := http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		io.WriteString(w, "ok") // nolint: errcheck
	})

	urls := common.NewURLPathPrefix("/")

	config := Config{
		AuthService:                "service",
		AuthSessionLifetimeSeconds: 3600,
		SessionAuthKey:             securecookie.GenerateRandomKey(64),
		SessionEncKey:              securecookie.GenerateRandomKey(32),
	}
	// h doubles as staticHandler.
	wh := doWrap(h, devMgr, ac, h, renderer, urls, &config)
	return httptest.NewServer(wh), ac
}

type step struct {
	requestFunc func(*httpClient) (string, string, error)
	checkFunc   func(string) bool
	expectedURI string
	expectErr   bool
}

func runStep(t *testing.T, c *httpClient, idx int, step step) {
	data, uri, err := step.requestFunc(c)
	if err != nil && !step.expectErr {
		t.Fatalf("error at step %d: unexpected error: %v", idx, err)
	}
	if err == nil && step.expectErr {
		t.Fatalf("error at step %d: was expecting an error but did not get one", idx)
	}
	if step.expectedURI != "" && step.expectedURI != uri {
		t.Errorf("error at step %d: bad response URI, got '%s' expected '%s'", idx, uri, step.expectedURI)
	}
	if step.checkFunc != nil && !step.checkFunc(data) {
		t.Fatalf("error at step %d: bad response '%s'", idx, data)
	}
}

func runConversation(t *testing.T, steps ...step) {
	srv, _ := startServer(t)
	defer srv.Close()

	c := newClient(srv.URL)

	for idx, step := range steps {
		runStep(t, c, idx, step)
	}
}

func requestSiteStep() step {
	return step{
		requestFunc: func(c *httpClient) (string, string, error) {
			return c.request("GET", "/", nil)
		},
		checkFunc: func(data string) bool {
			return isPage(data, "login") && (pageError(data) == "")
		},
		expectedURI: "/login/",
	}
}

func loginStep(username, password, expURI string, checkFunc func(string) bool) step {
	return step{
		requestFunc: func(c *httpClient) (string, string, error) {
			return c.request("POST", "/login/", formData(map[string]string{
				"username": username,
				"password": password,
				"r":        "/",
			}))
		},
		checkFunc:   checkFunc,
		expectedURI: expURI,
	}
}

func loginOKStep() step {
	return loginStep("testuser", "password", "/", isTargetWebsite)
}

func TestLogin_Ok(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginOKStep(),
	)
}

func TestLogin_Fail(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginStep("testuser", "wrong-password", "/login/", func(data string) bool {
			return isPage(data, "login") && (pageError(data) != "")
		}),
	)
}

func TestLogin_Ok_ShortCircuit(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginOKStep(),
		// Now ask for the login page again, expect to be
		// redirected directly to the main site.
		step{
			requestFunc: func(c *httpClient) (string, string, error) {
				return c.request("GET", "/login/?r=%2F", nil)
			},
			checkFunc:   isTargetWebsite,
			expectedURI: "/",
		},
		// Same, but with a POST request (no username or password provided).
		step{
			requestFunc: func(c *httpClient) (string, string, error) {
				return c.request("POST", "/login/", formData(map[string]string{
					"r": "/",
				}))
			},
			checkFunc:   isTargetWebsite,
			expectedURI: "/",
		},
	)
}

func loginOTPStep1() step {
	return loginStep("testuser-otp", "password", "/login/otp", func(data string) bool {
		return isPage(data, "login_otp") && (pageError(data) == "")
	})
}

func loginOTPStep2(otp, expURI string, checkFunc func(string) bool) step {
	return step{
		requestFunc: func(c *httpClient) (string, string, error) {
			return c.request("POST", "/login/otp", formData(map[string]string{
				"otp": otp,
				"r":   "/",
			}))
		},
		checkFunc:   checkFunc,
		expectedURI: expURI,
	}
}

func loginOTPOKStep(otp string) step {
	return loginOTPStep2(otp, "/", isTargetWebsite)
}

func TestLogin_OTP_Ok(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginOTPStep1(),
		loginOTPOKStep("123456"),
	)
}

func TestLogin_OTP_Fail(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginOTPStep1(),
		loginOTPStep2("234567", "/login/", func(data string) bool {
			return isPage(data, "login") && (pageError(data) != "")
		}),
	)
}

func TestLogin_OTP_FailSwitchToU2F(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginOTPStep1(),
		step{
			requestFunc: func(c *httpClient) (string, string, error) {
				return c.request("GET", "/login/u2f", nil)
			},
			checkFunc: func(data string) bool {
				return isPage(data, "login") && (pageError(data) != "")
			},
			expectedURI: "/login/",
		},
	)
}

func loginWebAuthnStep1() step {
	return loginStep("testuser-webauthn", "password", "/login/u2f", func(data string) bool {
		return isPage(data, "login_u2f") && (pageError(data) == "")
	})
}

func loginWebAuthnStep2() step {
	return step{
		requestFunc: func(c *httpClient) (string, string, error) {
			return c.request("POST", "/login/u2f", formData(map[string]string{
				"webauthn": testCredentialRequestBody,
				"r":        "/",
			}))
		},
		checkFunc: isTargetWebsite,
	}
}

func TestLogin_WebAuthn_Ok(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginWebAuthnStep1(),
		loginWebAuthnStep2(),
	)
}

func TestLogin_WebAuthn_Fail(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginWebAuthnStep1(),
		step{
			requestFunc: func(c *httpClient) (string, string, error) {
				return c.request("POST", "/login/u2f", strings.NewReader(""))
			},
			checkFunc: func(data string) bool {
				return isPage(data, "login_u2f") && (pageError(data) != "")
			},
		},
	)
}

func TestLogin_2FA_Switch_Ok(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginStep("testuser-both", "password", "/login/u2f", func(data string) bool {
			return isPage(data, "login_u2f") && (pageError(data) == "")
		}),
		// Switch to OTP directly.
		step{
			requestFunc: func(c *httpClient) (string, string, error) {
				return c.request("GET", "/login/otp", nil)
			},
			checkFunc: func(data string) bool {
				return isPage(data, "login_otp") && (pageError(data) == "")
			},
		},
		loginOTPOKStep("123456"),
	)
}

func TestLogin_SessionID_Is_Set(t *testing.T) {
	srv, ac := startServer(t)
	defer srv.Close()

	c := newClient(srv.URL)

	steps := []step{
		requestSiteStep(),
		loginOKStep(),
	}
	for idx, step := range steps {
		runStep(t, c, idx, step)
	}

	if ac.sessionID == "" {
		t.Fatal("session ID was not set during Authenticate() call")
	}
}

var testCredentialRequestBody = `{
	"id":"6xrtBhJQW6QU4tOaB4rrHaS2Ks0yDDL_q8jDC16DEjZ-VLVf4kCRkvl2xp2D71sTPYns-exsHQHTy3G-zJRK8g",
	"rawId":"6xrtBhJQW6QU4tOaB4rrHaS2Ks0yDDL_q8jDC16DEjZ-VLVf4kCRkvl2xp2D71sTPYns-exsHQHTy3G-zJRK8g",
	"type":"public-key",
	"response":{
                "authenticatorData":"dKbqkhPJnC90siSSsyDPQCYqlMGpUKA5fyklC2CEHvBFXJJiGa3OAAI1vMYKZIsLJfHwVQMANwCOw-atj9C0vhWpfWU-whzNjeQS21Lpxfdk_G-omAtffWztpGoErlNOfuXWRqm9Uj9ANJck1p6lAQIDJiABIVggKAhfsdHcBIc0KPgAcRyAIK_-Vi-nCXHkRHPNaCMBZ-4iWCBxB8fGYQSBONi9uvq0gv95dGWlhJrBwCsj_a4LJQKVHQ",
		"clientDataJSON":"eyJjaGFsbGVuZ2UiOiJFNFBUY0lIX0hmWDFwQzZTaWdrMVNDOU5BbGdlenROMDQzOXZpOHpfYzlrIiwibmV3X2tleXNfbWF5X2JlX2FkZGVkX2hlcmUiOiJkbyBub3QgY29tcGFyZSBjbGllbnREYXRhSlNPTiBhZ2FpbnN0IGEgdGVtcGxhdGUuIFNlZSBodHRwczovL2dvby5nbC95YWJQZXgiLCJvcmlnaW4iOiJodHRwczovL3dlYmF1dGhuLmlvIiwidHlwZSI6IndlYmF1dGhuLmdldCJ9",
		"signature":"MEUCIBtIVOQxzFYdyWQyxaLR0tik1TnuPhGVhXVSNgFwLmN5AiEAnxXdCq0UeAVGWxOaFcjBZ_mEZoXqNboY5IkQDdlWZYc"
		}
	}`
