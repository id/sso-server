package login

import (
	"log"
	"net/http"

	"git.autistici.org/id/sso-server/common"
)

func newLogoutHandler(authClient AuthClient, renderer *common.Renderer) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		svcList, ok := GetServiceList(req.Context())
		if ok {
			// Delete the cookie with the list of logged in services (but
			// the data in the svcList object stays there, so it can be
			// accessed from the template).
			if err := svcList.Delete(w); err != nil {
				log.Printf("logout error: deleting service_list cookie: %v", err)
			}
		} else {
			svcList = new(AuthServiceList)
		}

		// If the user is autenticated, invoke the Logout
		// method on the authentication client.
		if auth, ok := GetAuth(req.Context()); ok && auth.check() == nil {
			ctx := withSessionID(req.Context(), auth.SessionID)
			if err := authClient.Logout(ctx, auth.Username, auth.UserInfo); err != nil {
				log.Printf("logout error for %s: %v", auth.Username, err)
			}
		}

		// Set the custom CSP of the logout page.
		w.Header().Set("Content-Security-Policy", logoutCSP)

		renderer.Render(w, req, "logout.html", map[string]interface{}{
			"ServiceList": svcList,
		})
	})
}
