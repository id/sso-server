package login

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"git.autistici.org/id/auth"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

func normalizeUsername(s string) string {
	return strings.ToLower(strings.TrimSpace(s))
}

func authRequestDebugString(req *auth.Request) string {
	s := fmt.Sprintf("user=%s", req.Username)
	if len(req.Password) > 0 {
		s += ",password"
	}
	if req.OTP != "" {
		s += ",otp"
	}
	if req.WebAuthnResponse != nil {
		s += ",webauthn"
	}
	if req.DeviceInfo != nil && len(req.DeviceInfo.ID) > 5 {
		s += fmt.Sprintf(",device=%s", req.DeviceInfo.ID[:5])
	}
	return s
}

func authResponseDebugString(resp *auth.Response) string {
	s := fmt.Sprintf("status=%s", resp.Status.String())
	switch resp.Status {
	case auth.StatusOK:
		// if resp.Mechanism != "" {
		// 	s += fmt.Sprintf(",how=%s", resp.Mechanism)
		// }
		if resp.UserInfo != nil {
			if len(resp.UserInfo.Groups) > 0 {
				s += fmt.Sprintf(",groups=[%s]", strings.Join(resp.UserInfo.Groups, ","))
			}
			if resp.UserInfo.Shard != "" {
				s += fmt.Sprintf(",shard=%s", resp.UserInfo.Shard)
			}
		}
	case auth.StatusInsufficientCredentials:
		s += ",tfa_methods=["
		for idx, m := range resp.TFAMethods {
			if idx > 0 {
				s += ","
			}
			s += string(m)
		}
		s += "]"
	}
	return s
}

// AuthClient wrapper that is bound to a specific service.
type serviceBoundAuthClient struct {
	AuthClient

	service string
}

func newServiceBoundAuthClient(c AuthClient, service string) *serviceBoundAuthClient {
	return &serviceBoundAuthClient{
		AuthClient: c,
		service:    service,
	}
}

func (c *serviceBoundAuthClient) Authenticate(ctx context.Context, req *auth.Request) (*auth.Response, error) {
	req.Service = c.service
	return c.AuthClient.Authenticate(ctx, req)
}

// AuthClient wrapper that annotates the RPC trace with authentication call spans.
type tracingAuthClient struct {
	AuthClient
}

func newTracingAuthClient(c AuthClient) *tracingAuthClient {
	return &tracingAuthClient{AuthClient: c}
}

func (c *tracingAuthClient) Authenticate(ctx context.Context, req *auth.Request) (*auth.Response, error) {
	// Trace the authentication request.
	ctx, span := otel.GetTracerProvider().Tracer("login").Start(
		ctx, "auth", trace.WithSpanKind(trace.SpanKindClient), trace.WithAttributes(
			attribute.String("auth.user", req.Username),
			attribute.String("auth.service", req.Service),
			attribute.Bool("auth.with_password", len(req.Password) > 0),
			attribute.Bool("auth.with_otp", req.OTP != ""),
			attribute.Bool("auth.with_u2f", req.WebAuthnResponse != nil),
		))
	defer span.End()

	resp, err := c.AuthClient.Authenticate(ctx, req)

	// Record the authentication response status in the trace.
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
	} else if resp.Status == auth.StatusOK {
		span.SetStatus(codes.Ok, "OK")
	} else {
		span.SetStatus(codes.Error, resp.Status.String())
	}

	return resp, err
}

func parseSameSiteMode(s string) (http.SameSite, error) {
	switch strings.ToLower(s) {
	case "", "lax":
		return http.SameSiteLaxMode, nil
	case "strict":
		return http.SameSiteStrictMode, nil
	case "none":
		return http.SameSiteNoneMode, nil
	}
	return 0, errors.New("unknown SameSite policy")
}
