package common

import (
	crand "crypto/rand"
	"encoding/binary"
	"math/rand"
)

// RandomBytes returns n bytes from crypt/rand.Reader.
func RandomBytes(n int) []byte {
	b := make([]byte, n)
	if _, err := crand.Read(b[:]); err != nil {
		panic(err)
	}
	return b
}

func newSeed() int64 {
	u := binary.BigEndian.Uint64(RandomBytes(8))
	return int64(u & 0x7fffffffffffffff)
}

// WeakRandomSource returns a weak math/rand RNG (non-crypto-grade)
// initialized with a random seed.
func WeakRandomSource() *rand.Rand {
	// We're fine with using a weak RNG here, it's literally the
	// point of this function.
	// nolint: gosec
	return rand.New(rand.NewSource(newSeed()))
}
