package common

import (
	"errors"

	"gopkg.in/yaml.v3"
)

// YAMLBytes is a []byte slice that can be safely unmarshaled from
// YAML. For the time being, the value is just decoded as-is, but for
// full binary blob support we might switch to expect base64-encoding.
type YAMLBytes []byte

// UnmarshalYAML implements the yaml.Unmarshaler interface.
func (b *YAMLBytes) UnmarshalYAML(value *yaml.Node) error {
	var s string
	if err := value.Decode(&s); err != nil {
		return err
	}
	*b = []byte(s)
	return nil
}

// SessionAuthenticationKey is a binary type for gorilla.session
// authentication keys, that is YAML-unserializable.
type SessionAuthenticationKey YAMLBytes

func (k *SessionAuthenticationKey) UnmarshalYAML(value *yaml.Node) error {
	if err := (*YAMLBytes)(k).UnmarshalYAML(value); err != nil {
		return err
	}
	switch len(*k) {
	case 32, 64:
	default:
		return errors.New("session authentication keys must be exactly 32 or 64 bytes long")
	}
	return nil
}

// SessionEncryptionKey is a binary type for gorilla.session
// encryption keys, that is YAML-unserializable.
type SessionEncryptionKey YAMLBytes

func (k *SessionEncryptionKey) UnmarshalYAML(value *yaml.Node) error {
	if err := (*YAMLBytes)(k).UnmarshalYAML(value); err != nil {
		return err
	}
	switch len(*k) {
	case 16, 24, 32:
	default:
		return errors.New("session encryption keys must be exactly 16, 24 or 32 bytes long")
	}
	return nil
}
