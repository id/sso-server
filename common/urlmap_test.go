package common

import "testing"

func TestURLMap(t *testing.T) {
	m := NewURLPathPrefix("/sso/")

	real := m.URLFor("/login/u2f")
	if real != "/sso/login/u2f" {
		t.Errorf("mapped URL is %s, expected /sso/login/u2f", real)
	}
	resolved := m.Resolve(real)
	if resolved != "/login/u2f" {
		t.Errorf("resolved URL is %s, expected /login/u2f", resolved)
	}
}
