package common

import "strings"

// URLMap is an interface between our logical view of application URLs
// and the user's. The only implementation allows us to serve the app
// below its own URL path prefix.
type URLMap interface {
	URLFor(string) string
	Resolve(string) string
}

type urlPathPrefix string

// NewURLPathPrefix creates a URLMap where everything is served under
// a path prefix.
func NewURLPathPrefix(prefix string) URLMap {
	return urlPathPrefix(strings.TrimRight(prefix, "/"))
}

func (p urlPathPrefix) URLFor(u string) string {
	return string(p) + u
}

func (p urlPathPrefix) Resolve(u string) string {
	return u[len(p):]
}
