package common

import "net/http"

// WithDynamicHeaders wraps an http.Handler with cache-busting and
// security-related headers appropriate for a user-facing dynamic
// application. The 'csp' argument sets a default
// Content-Security-Policy.
func WithDynamicHeaders(h http.Handler, csp string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		hdr := w.Header()
		hdr.Set("Pragma", "no-cache")
		hdr.Set("Cache-Control", "no-store")
		hdr.Set("Expires", "-1")
		hdr.Set("X-Frame-Options", "DENY")
		hdr.Set("X-XSS-Protection", "1; mode=block")
		hdr.Set("X-Content-Type-Options", "nosniff")
		if csp != "" && hdr.Get("Content-Security-Policy") == "" {
			hdr.Set("Content-Security-Policy", csp)
		}
		h.ServeHTTP(w, r)
	})
}
