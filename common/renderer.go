package common

import (
	"bytes"
	"html/template"
	"io"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/csrf"
)

// A Renderer just renders HTML templates with some common context
// variables. Context is represented as a map[string]interface{}, to
// allow the merge operation.
type Renderer struct {
	tpl  *template.Template
	vars map[string]interface{}
}

// NewRenderer creates a new Renderer with the provided templates and
// default variables.
func NewRenderer(tpl *template.Template, vars map[string]interface{}) *Renderer {
	return &Renderer{
		tpl:  tpl,
		vars: vars,
	}
}

// Render the named HTML template to 'w'.
func (r *Renderer) Render(w http.ResponseWriter, req *http.Request, templateName string, data map[string]interface{}) {
	// Merge default variables with the ones passed in 'data',
	// without modifying either. Always populate the CRSFField
	// variable with the current CSRF token.
	vars := make(map[string]interface{})
	vars["CSRFField"] = csrf.TemplateField(req)
	for k, v := range r.vars {
		vars[k] = v
	}
	for k, v := range data {
		vars[k] = v
	}

	// Render the template into a buffer, to prevent returning
	// half-rendered templates when there is an error.
	var buf bytes.Buffer
	if err := r.tpl.ExecuteTemplate(&buf, templateName, vars); err != nil {
		log.Printf("template rendering error for %s: %v", req.URL.String(), err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Write our response to the client.
	w.Header().Set("Content-Length", strconv.Itoa(buf.Len()))
	io.Copy(w, &buf) // nolint
}
