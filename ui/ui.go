package ui

//go:generate npm install
//go:generate env PROD=1 ./node_modules/.bin/webpack
//go:generate go-bindata --nocompress --pkg ui --prefix assets/ assets/...

import (
	"bytes"
	"encoding/json"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"git.autistici.org/id/sso-server/common"
	assetfs "github.com/elazarl/go-bindata-assetfs"
)

type Config struct {
	AccountRecoveryURL string `yaml:"account_recovery_url"`
	LoginAgainURL      string `yaml:"login_again_url"`
	SiteName           string `yaml:"site_name"`
	SiteLogo           string `yaml:"site_logo"`
	SiteFavicon        string `yaml:"site_favicon"`
	LoginUsernameLabel string `yaml:"login_username_label"`
}

func Build(config *Config, urls common.URLMap) (http.Handler, *common.Renderer, error) {
	label := "Username"
	if config.LoginUsernameLabel != "" {
		label = config.LoginUsernameLabel
	}
	renderer := common.NewRenderer(
		parseEmbeddedTemplates(),
		map[string]interface{}{
			"AccountRecoveryURL": config.AccountRecoveryURL,
			"LoginAgainURL":      config.LoginAgainURL,
			"SiteName":           config.SiteName,
			"SiteLogo":           config.SiteLogo,
			"SiteFavicon":        config.SiteFavicon,
			"LoginUsernameLabel": label,
			"URLMap":             urls,
		},
	)

	mux := http.NewServeMux()

	// Load optional custom components that will override the
	// default static content embedded in the app.
	if config.SiteLogo != "" {
		siteLogo, err := loadStaticContent(config.SiteLogo)
		if err != nil {
			return nil, nil, err
		}
		mux.Handle(urls.URLFor("/static/img/site_logo"), siteLogo)
	}
	if config.SiteFavicon != "" {
		siteFavicon, err := loadStaticContent(config.SiteFavicon)
		if err != nil {
			return nil, nil, err
		}
		mux.Handle(urls.URLFor("/static/favicon.ico"), siteFavicon)
	}

	mux.Handle(urls.URLFor("/static/"), http.StripPrefix(urls.URLFor("/static/"), http.FileServer(&assetfs.AssetFS{
		Asset:     Asset,
		AssetDir:  AssetDir,
		AssetInfo: AssetInfo,
		Prefix:    "static",
	})))

	// Add cache-friendly headers.
	h := withCacheHeaders(mux)

	return h, renderer, nil
}

// Parse the templates that are embedded with the binary (in bindata.go).
func parseEmbeddedTemplates() *template.Template {
	root := template.New("").Funcs(map[string]interface{}{
		"toJSON": toJSON,
	})
	files, err := AssetDir("templates")
	if err != nil {
		log.Fatalf("no asset dir for templates: %v", err)
	}
	for _, f := range files {
		b, err := Asset("templates/" + f)
		if err != nil {
			log.Fatalf("could not read embedded template %s: %v", f, err)
		}
		if _, err := root.New(f).Parse(string(b)); err != nil {
			log.Fatalf("error parsing template %s: %v", f, err)
		}
	}
	return root
}

// staticContent is an http.Handler that serves in-memory data as if
// it were a static file.
type staticContent struct {
	modtime time.Time
	name    string
	data    []byte
}

// loadStaticContent creates a StaticContent by loading data from a file.
func loadStaticContent(path string) (*staticContent, error) {
	stat, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadFile(path) // #nosec
	if err != nil {
		return nil, err
	}
	return &staticContent{
		name:    path,
		modtime: stat.ModTime(),
		data:    data,
	}, nil
}

func (c *staticContent) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	http.ServeContent(w, req, c.name, c.modtime, bytes.NewReader(c.data))
}

func toJSON(obj interface{}) string {
	data, _ := json.Marshal(obj)
	return string(data)
}

type cachedResponseWriter struct {
	http.ResponseWriter
}

func (c *cachedResponseWriter) WriteHeader(statusCode int) {
	if statusCode == http.StatusOK {
		c.ResponseWriter.Header().Set("Cache-Control", "max-age=86400")
	}
	c.ResponseWriter.WriteHeader(statusCode)
}

func withCacheHeaders(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		cw := &cachedResponseWriter{ResponseWriter: w}
		h.ServeHTTP(cw, req)
	})
}
