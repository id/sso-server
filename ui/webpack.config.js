const path = require('path');
const glob = require('glob');
const webpack = require('webpack');
const PROD = JSON.parse(process.env.PROD || '0');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { PurgeCSSPlugin } = require('purgecss-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { SubresourceIntegrityPlugin } = require('webpack-subresource-integrity');

const TEMPLATES = glob.sync(path.join(__dirname, 'templates/*.html'));

module.exports = {
    context: __dirname + '/src',
    entry: {
        // Define all files in src/ as sources, everything except sso.js and
        // sso.css will be copied as-is to the static asset directory.
        sso: glob.sync(path.join(__dirname, 'src/*')).map(el => {
            return './' + path.basename(el);
        }),
    },
    mode: PROD ? 'production' : 'development',
    output: {
        publicPath: './',
        path: __dirname + '/assets',
        filename: 'static/[name].[contenthash].js',
        assetModuleFilename: 'static/[name][ext]',
        clean: true,
        crossOriginLoading: 'anonymous',
        hashFunction: 'sha256',
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: 'static/[name].[contenthash].css',
        }),
        new PurgeCSSPlugin({
            paths: TEMPLATES,
            safelist: [/^logout-status/],
        }),
        new SubresourceIntegrityPlugin(),
    ].concat(
        // Process all templates through HtmlWebpackPlugin, to inject
        // SRI-aware links to the webpack-generated resources.
        glob.sync(path.join(__dirname, 'templates/*.html')).map(f => new HtmlWebpackPlugin({
            filename: path.join('templates', path.basename(f)),
            template: f,
            minify: false,
            inject: false,
        }))),

    module: {
        rules: [
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
                sideEffects: true,
            },
            {
                test: /\.(ico|png|gif|jpe?g|svg|eot|otf|ttf|woff)$/,
                type: 'asset/resource',
            },
        ],
    },
};
