var idu2f = {};

idu2f.get_sign_request = function() {
    return JSON.parse($('meta[name=u2f_request]').attr('value'));
};

idu2f.show_message = function(msg) {
    $('#u2fMsg').text(msg).show();
};

idu2f.is_error = function(resp) {
    if (!('errorCode' in resp)) {
        return false;
    }
    if (resp.errorCode === u2f.ErrorCodes['OK']) {
        return false;
    }
    return true;
};

idu2f.error_message = function(resp) {
    var msg = 'U2F error code ' + resp.errorCode;
    for (name in u2f.ErrorCodes) {
        if (u2f.ErrorCodes[name] === resp.errorCode) {
            msg += ' (' + name + ')';
        }
    }
    if (resp.errorMessage) {
        msg += ': ' + resp.errorMessage;
    }
    return msg;
};

idu2f.sign_callback = function(resp) {
    if (idu2f.is_error(resp)) {
        idu2f.show_message(idu2f.error_message(resp));
        return;
    }
    idu2f.show_message('Touch received, sending to server...');
    $('#u2fResponseField').val(JSON.stringify(resp));
    $('#u2fForm').submit();
};

idu2f.sign = function() {
    var request = idu2f.get_sign_request();
    console.log('Waiting for touch...');
    u2f.sign(request.appId, request.challenge, request.registeredKeys, idu2f.sign_callback, 300);
};

$(function() {
    idu2f.sign();
});
