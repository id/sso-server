package server

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/cors"

	authclient "git.autistici.org/id/auth/client"

	"git.autistici.org/id/sso-server/common"
	"git.autistici.org/id/sso-server/login"
)

// Returns the URL of the login handler on the target service.
func serviceLoginCallback(service, destination, token string) string {
	v := make(url.Values)
	v.Set("t", token)
	v.Set("d", destination)
	return fmt.Sprintf("https://%ssso_login?%s", service, v.Encode())
}

// Server for the SSO protocol. Provides the HTTP interface to a
// LoginService, protected by the login application.
type httpServer struct {
	loginService        *LoginService
	homepageRedirectURL string
}

// New returns a new Server.
func New(loginService *LoginService, authClient authclient.Client, config *Config) (http.Handler, error) {
	srv := &httpServer{
		loginService:        loginService,
		homepageRedirectURL: config.DefaultSignedInRedirect,
	}

	urls := common.NewURLPathPrefix(config.URLPrefix)
	rootPath := urls.URLFor("/")
	h := http.Handler(http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if req.URL.Path == rootPath || req.URL.Path == strings.TrimRight(rootPath, "/") {
			srv.handleGrantTicket(w, req)
			return
		}
		http.NotFound(w, req)
	}))
	h = common.WithDynamicHeaders(h, "")

	ac, err := authClientFromConfig(authClient, config)
	if err != nil {
		return nil, err
	}

	// Protect the main endpoint with the login application.
	wrapped, err := login.Wrap(h, ac, &config.Config, urls)
	if err != nil {
		return nil, err
	}

	// Add the /exchange endpoint (which does not use the normal
	// HTTP-based login workflow) in front of everything.
	exchangePath := urls.URLFor("/exchange")
	h = http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if req.URL.Path == exchangePath {
			srv.handleExchange(w, req)
			return
		}
		wrapped.ServeHTTP(w, req)
	})

	// Add CORS headers on the main IDP endpoints.
	corsp := cors.New(cors.Options{
		AllowedOrigins:   config.AllowedCORSOrigins,
		AllowedHeaders:   []string{"*"},
		AllowCredentials: true,
		MaxAge:           86400,
	})
	h = corsp.Handler(h)

	return h, nil
}

// Token signing handler. Authorizes an authenticated user to a service by
// signing a token with the user's identity. The client is redirected back to
// the original service, with the signed token.
func (h *httpServer) handleGrantTicket(w http.ResponseWriter, req *http.Request) {
	// Extract the authorization request parameters from the HTTP
	// request query args.
	//
	// *NOTE*: we do not want to parse the request body, in case
	// it is a POST request redirected from a 307, so we do not
	// call req.FormValue() but look directly into request.URL
	// instead.
	auth, ok := login.GetAuth(req.Context())
	if !ok {
		http.Error(w, "No valid session", http.StatusBadRequest)
		return
	}

	username := auth.Username
	queryArgs := req.URL.Query()
	service := queryArgs.Get("s")
	destination := queryArgs.Get("d")
	nonce := queryArgs.Get("n")
	groupsStr := queryArgs.Get("g")

	// If the above parameters are unset, we're probably faced with a user
	// that reached this URL by other means. Redirect them to the
	// configured homepageRedirectURL, or at least return a slightly more
	// user-friendly error.
	if service == "" || destination == "" {
		if h.homepageRedirectURL != "" {
			http.Redirect(w, req, h.homepageRedirectURL, http.StatusFound)
		} else {
			http.Error(w, "You are not supposed to reach this page directly. Use the back button in your browser instead.", http.StatusBadRequest)
		}
		return
	}

	// Compute the intersection of the user's groups and the
	// requested groups, to obtain the group memberships to grant.
	var groups []string
	if groupsStr != "" {
		reqGroups := strings.Split(groupsStr, ",")
		if len(reqGroups) > 0 {
			if auth.UserInfo != nil {
				groups = intersectGroups(reqGroups, auth.UserInfo.Groups)
			}
			// We only make this check here as a convenience to
			// the user (we may be able to show a nicer UI): the
			// actual group ACL must be applied on the destination
			// service, because the 'g' parameter is untrusted at
			// this stage.
			if len(groups) == 0 {
				http.Error(w, "Forbidden", http.StatusForbidden)
				return
			}
		}
	}

	// Make the authorization request. Tickets should not live
	// longer than the authentication session.
	ttl := auth.Deadline.Sub(time.Now().UTC())
	token, err := h.loginService.Authorize(username, service, destination, nonce, groups, ttl)
	if err != nil {
		log.Printf("auth error: %v: user=%s service=%s destination=%s nonce=%s groups=%s", err, username, service, destination, nonce, groupsStr)
		http.Error(w, err.Error(), http.StatusBadRequest)
		errorsCounter.WithLabelValues(service, "false").Inc()
		return
	}

	log.Printf("authorized %s for %s (ttl=%ds)", username, service, int(ttl.Seconds()))
	grantsCounter.WithLabelValues(service, "false").Inc()

	// Record the service in the session.
	if svcList, ok := login.GetServiceList(req.Context()); ok {
		svcList.AddService(service)
		if err := svcList.Save(w); err != nil {
			log.Printf("error saving service list cookie: %v", err)
		}
	}

	// Redirect to service callback.
	callbackURL := serviceLoginCallback(service, destination, token)
	http.Redirect(w, req, callbackURL, http.StatusFound)
}

func (h *httpServer) handleExchange(w http.ResponseWriter, req *http.Request) {
	curToken := req.FormValue("cur_tkt")
	curService := req.FormValue("cur_svc")
	curNonce := req.FormValue("cur_nonce")
	newService := req.FormValue("new_svc")
	newNonce := req.FormValue("new_nonce")

	token, err := h.loginService.Exchange(curToken, curService, curNonce, newService, newNonce)
	switch {
	case errors.Is(err, ErrUnauthorized):
		log.Printf("unauthorized exchange request (%s -> %s)", curService, newService)
		http.Error(w, "Forbidden", http.StatusForbidden)
		errorsCounter.WithLabelValues(newService, "true").Inc()
		return
	case err != nil:
		log.Printf("exchange error (%s -> %s): %v", curService, newService, err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		errorsCounter.WithLabelValues(newService, "true").Inc()
		return
	}

	grantsCounter.WithLabelValues(newService, "true").Inc()
	w.Header().Set("Content-Type", "text/plain")
	io.WriteString(w, token) // nolint
}

// Returns true if the intersection of the sets isn't empty (in O(N^2)
// time).
func inAnyGroups(groups, ref []string) bool {
	for _, rr := range ref {
		for _, gg := range groups {
			if gg == rr {
				return true
			}
		}
	}
	return false
}

// Returns the intersection of two string lists (in O(N^2) time).
func intersectGroups(a, b []string) []string {
	var out []string
	for _, aa := range a {
		for _, bb := range b {
			if aa == bb {
				out = append(out, aa)
				break
			}
		}
	}
	return out
}

// Prometheus instrumentation.
var (
	grantsCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "sso_grants_total",
		Help: "Counter of ticket grants by service.",
	}, []string{"service", "exchange"})
	errorsCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "sso_grant_errors_total",
		Help: "Counter of authorization errors by service.",
	}, []string{"service", "exchange"})
	keystoreCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "sso_keystore_unlocks_total",
		Help: "Counter of keystore unlocks.",
	}, []string{"status", "shard"})
)

func init() {
	prometheus.MustRegister(grantsCounter)
	prometheus.MustRegister(errorsCounter)
	prometheus.MustRegister(keystoreCounter)
}
