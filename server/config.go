package server

import (
	"errors"
	"regexp"
	"strings"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/id/sso-server/login"
)

// Config data for the SSO service.
type Config struct {
	login.Config `yaml:",inline"`

	URLPrefix string `yaml:"url_path_prefix"`

	AllowedCORSOrigins []string `yaml:"allowed_cors_origins"`

	SecretKeyFile    string   `yaml:"secret_key_file"`
	PublicKeyFile    string   `yaml:"public_key_file"`
	Domain           string   `yaml:"domain"`
	AllowedServices  []string `yaml:"allowed_services"`
	AllowedExchanges []*struct {
		SrcRegexp string `yaml:"src_regexp"`
		DstRegexp string `yaml:"dst_regexp"`
		srcRx     *regexp.Regexp
		dstRx     *regexp.Regexp
	} `yaml:"allowed_exchanges"`
	ServiceTTLs []*struct {
		Regexp     string `yaml:"regexp"`
		TTLSeconds int    `yaml:"ttl"`
		rx         *regexp.Regexp
	} `yaml:"service_ttls"`

	KeyStore             *clientutil.BackendConfig `yaml:"keystore"`
	KeyStoreEnableGroups []string                  `yaml:"keystore_enable_groups"`

	LoginDelayMs float64 `yaml:"login_delay_ms"`

	allowedServicesRx []*regexp.Regexp
}

// Check syntax (missing required values).
func (c *Config) valid() error {
	switch {
	case c.SecretKeyFile == "":
		return errors.New("secret_key_file is empty")
	case c.PublicKeyFile == "":
		return errors.New("public_key_file is empty")
	case c.Domain == "":
		return errors.New("domain is empty")
	case len(c.AllowedServices) == 0:
		return errors.New("the list of allowed services is empty")
	case c.AuthService == "":
		return errors.New("auth_service is empty")
	case c.URLPrefix != "" && !strings.HasPrefix(c.URLPrefix, "/"):
		return errors.New("url_path_prefix does not start with /")
	}
	return nil
}

// Compile the configuration (parse regular expressions, etc).
func (c *Config) Compile() error {
	err := c.valid()
	if err != nil {
		return err
	}

	for _, svcttl := range c.ServiceTTLs {
		svcttl.rx, err = regexp.Compile(svcttl.Regexp)
		if err != nil {
			return err
		}
	}

	for _, xch := range c.AllowedExchanges {
		xch.srcRx, err = regexp.Compile(xch.SrcRegexp)
		if err != nil {
			return err
		}
		xch.dstRx, err = regexp.Compile(xch.DstRegexp)
		if err != nil {
			return err
		}
	}

	for _, pattern := range c.AllowedServices {
		rx, err := regexp.Compile(pattern)
		if err != nil {
			return err
		}
		c.allowedServicesRx = append(c.allowedServicesRx, rx)
	}

	return nil
}

var defaultServiceTTL = 300 * time.Second

func (c *Config) getServiceTTL(service string) time.Duration {
	for _, svcttl := range c.ServiceTTLs {
		if svcttl.rx.MatchString(service) {
			return time.Duration(svcttl.TTLSeconds) * time.Second
		}
	}
	return defaultServiceTTL
}

func (c *Config) isServiceAllowed(service string) bool {
	for _, rx := range c.allowedServicesRx {
		if rx.MatchString(service) {
			return true
		}
	}
	return false
}

func (c *Config) isExchangeAllowed(src, dst string) bool {
	for _, xch := range c.AllowedExchanges {
		if xch.srcRx.MatchString(src) && xch.dstRx.MatchString(dst) {
			return true
		}
	}
	return false
}
