package server

import (
	"context"
	"log"
	"math/rand"
	"time"

	"git.autistici.org/id/auth"
	authclient "git.autistici.org/id/auth/client"
	ksclient "git.autistici.org/id/keystore/client"
	"git.autistici.org/id/sso-server/common"
	"git.autistici.org/id/sso-server/login"
)

type authClientAdapter struct {
	authclient.Client
}

func (c *authClientAdapter) Logout(context.Context, string, *auth.UserInfo) error {
	return nil
}

func authClientFromConfig(client authclient.Client, config *Config) (login.AuthClient, error) {
	var ac login.AuthClient = &authClientAdapter{Client: client}

	if config.KeyStore != nil {
		ks, err := ksclient.New(config.KeyStore)
		if err != nil {
			return nil, err
		}
		ac = &keystoreAuthClient{
			AuthClient:  ac,
			keystore:    ks,
			groups:      config.KeyStoreEnableGroups,
			authTTLSecs: config.AuthSessionLifetimeSeconds,
		}
		log.Printf("keystore client enabled")
	}

	if config.LoginDelayMs > 0 {
		ac = withRandomDelay(ac, config.LoginDelayMs)
	}

	return ac, nil
}

type keystoreAuthClient struct {
	login.AuthClient

	keystore    ksclient.Client
	groups      []string
	authTTLSecs int
}

func (k *keystoreAuthClient) Authenticate(ctx context.Context, req *auth.Request) (*auth.Response, error) {
	resp, err := k.AuthClient.Authenticate(ctx, req)

	if err == nil && resp.Status == auth.StatusOK {
		// Enable key store RPC only if groups are either not defined,
		// or they match one of the user's memberships.
		if (len(k.groups) == 0) || (resp.UserInfo != nil && inAnyGroups(resp.UserInfo.Groups, k.groups)) {
			var shard string
			if resp.UserInfo != nil {
				shard = resp.UserInfo.Shard
			}

			// Add 10 minutes of buffer to the auth session TTL.
			ttl := k.authTTLSecs + 600

			// Fetch the session ID from context.
			sessionID, _ := login.GetSessionID(ctx)

			err = k.keystore.Open(ctx, shard, req.Username, string(req.Password), sessionID, ttl)

			if err == nil {
				keystoreCounter.WithLabelValues("ok", shard).Inc()
			} else {
				keystoreCounter.WithLabelValues("error", shard).Inc()
			}
		}
	}

	return resp, err
}

func (k *keystoreAuthClient) Logout(ctx context.Context, username string, info *auth.UserInfo) error {
	err := k.AuthClient.Logout(ctx, username, info)

	if (len(k.groups) == 0) || (info != nil && inAnyGroups(info.Groups, k.groups)) {
		var shard string
		if info != nil {
			shard = info.Shard
		}

		sessionID, _ := login.GetSessionID(ctx)

		if kerr := k.keystore.Close(ctx, shard, username, sessionID); kerr != nil {
			// This is not a fatal error.
			log.Printf("warning: failed to wipe keystore for user %s: %v", username, err)
		}
	}

	return err
}

type delayedAuthClient struct {
	login.AuthClient

	rnd                   *rand.Rand
	delayBase, delayRange float64
}

func withRandomDelay(wrap login.AuthClient, delayMs float64) login.AuthClient {
	return &delayedAuthClient{
		AuthClient: wrap,
		delayBase:  delayMs,
		delayRange: delayMs,

		// We're fine with a weak RNG here, it's just used to
		// randomize delays a little bit.
		// nolint: gosec
		rnd: common.WeakRandomSource(),
	}
}

func (l *delayedAuthClient) Authenticate(ctx context.Context, req *auth.Request) (*auth.Response, error) {
	resp, err := l.AuthClient.Authenticate(ctx, req)
	sleepMs := l.delayBase - (l.delayRange * (l.rnd.Float64() - 0.5))
	time.Sleep(time.Duration(sleepMs) * time.Millisecond)
	return resp, err
}
