package server

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"net/url"
	"os"
	"regexp"
	"strings"
	"testing"

	"git.autistici.org/id/auth"
	"git.autistici.org/id/keystore"
)

type fakeAuthClient struct{}

func (c *fakeAuthClient) Authenticate(_ context.Context, req *auth.Request) (*auth.Response, error) {
	p := string(req.Password)
	info := &auth.UserInfo{
		Shard:  "shard1",
		Groups: []string{"users"},
	}
	switch {
	case req.Username == "testuser" && p == "password":
		return &auth.Response{Status: auth.StatusOK, UserInfo: info}, nil
	case req.Username == "test2fa" && p == "password" && req.OTP == "123456":
		return &auth.Response{Status: auth.StatusOK, UserInfo: info}, nil
	case req.Username == "test2fa" && p == "password" && req.OTP == "":
		return &auth.Response{
			Status:     auth.StatusInsufficientCredentials,
			TFAMethods: []auth.TFAMethod{auth.TFAMethodOTP},
		}, nil
	}

	return &auth.Response{Status: auth.StatusError}, nil
}

func createTestHTTPServer(t testing.TB, config *Config) *httptest.Server {
	svc, err := NewLoginService(config)
	if err != nil {
		t.Fatal("NewLoginService():", err)
	}

	srv, err := New(svc, &fakeAuthClient{}, config)
	if err != nil {
		t.Fatal("New():", err)
	}

	return httptest.NewTLSServer(srv)
}

func startTestHTTPServerWithConfig(t testing.TB) (string, *httptest.Server, *Config) {
	tmpdir, _ := ioutil.TempDir("", "")
	config := testConfig(t, tmpdir, "")
	return tmpdir, createTestHTTPServer(t, config), config
}

func startTestHTTPServer(t testing.TB) (string, *httptest.Server) {
	tmpdir, srv, _ := startTestHTTPServerWithConfig(t)
	return tmpdir, srv
}

func startTestHTTPServerWithKeyStore(t testing.TB) (string, *httptest.Server, *fakeKeyStore) {
	ks := createFakeKeyStore(t, "testuser", "password")

	tmpdir, _ := ioutil.TempDir("", "")
	config := testConfig(t, tmpdir, ks.URL)
	return tmpdir, createTestHTTPServer(t, config), ks
}

func makeHTTPClient(dnsOverrides map[string]string, followExternalRedirects bool) *http.Client {
	jar, _ := cookiejar.New(nil)
	var dialfn func(ctx context.Context, network, addr string) (net.Conn, error)
	if dnsOverrides != nil {
		dialer := new(net.Dialer)
		dialfn = func(ctx context.Context, network, addr string) (net.Conn, error) {
			if override, ok := dnsOverrides[addr]; ok {
				addr = override
			}
			return dialer.DialContext(ctx, network, addr)
		}
	}
	transport := NewLoggedTransport(&http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
		DialContext: dialfn,
	}, DefaultLogger{Dump: true})
	return &http.Client{
		Jar:       jar,
		Transport: transport,
		// This client will only follow redirects to localhost.
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			if len(via) > 10 {
				return errors.New("too many redirects")
			}
			if !followExternalRedirects && !strings.HasPrefix(req.URL.Host, "127.0.0.1:") {
				return http.ErrUseLastResponse
			}
			return nil
		},
	}
}

func newTestHTTPClient() *http.Client {
	return makeHTTPClient(nil, false)
}

func TestHTTP_ServeStaticAsset(t *testing.T) {
	tmpdir, httpSrv := startTestHTTPServer(t)
	defer os.RemoveAll(tmpdir)
	defer httpSrv.Close()

	c := newTestHTTPClient()
	resp, err := c.Get(httpSrv.URL + "/static/favicon.ico")
	if err != nil {
		t.Fatal("http.Get():", err)
	}
	if resp.StatusCode != 200 {
		t.Fatalf("bad status: %s", resp.Status)
	}
}

func doGet(t testing.TB, c *http.Client, uri string, checkResponse ...func(testing.TB, *http.Response)) {
	resp, err := c.Get(uri)
	if err != nil {
		t.Fatalf("http.Get(%s): %v", uri, err)
	}
	body := resp.Body
	defer body.Close()

	data, err := ioutil.ReadAll(body)
	if err != nil {
		t.Fatalf("http.Get(%s): %v", uri, err)
	}
	for _, f := range checkResponse {
		resp.Body = ioutil.NopCloser(bytes.NewReader(data))
		f(t, resp)
	}
}

func doPostForm(t testing.TB, c *http.Client, uri string, v url.Values, checkResponse ...func(testing.TB, *http.Response)) {
	resp, err := c.PostForm(uri, v)
	if err != nil {
		t.Fatalf("http.Get(%s): %v", uri, err)
	}
	body := resp.Body
	defer body.Close()

	data, err := ioutil.ReadAll(body)
	if err != nil {
		t.Fatalf("http.Get(%s): %v", uri, err)
	}
	for _, f := range checkResponse {
		resp.Body = ioutil.NopCloser(bytes.NewReader(data))
		f(t, resp)
	}
}

func checkStatusOk(t testing.TB, resp *http.Response) {
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		t.Fatalf("expected status 200, got %s", resp.Status)
	}
}

func checkStatusForbidden(t testing.TB, resp *http.Response) {
	if resp.StatusCode != 403 {
		t.Fatalf("expected status 403, got %s", resp.Status)
	}
}

func checkStatusNotFound(t testing.TB, resp *http.Response) {
	if resp.StatusCode != 404 {
		t.Fatalf("expected status 404, got %s", resp.Status)
	}
}

func checkRedirectToTargetService(t testing.TB, resp *http.Response) {
	if resp.StatusCode != 302 {
		t.Fatalf("expected status 302, got %s", resp.Status)
	}
	if !strings.HasPrefix(resp.Header.Get("Location"), "https://service.example.com/sso_login?") {
		t.Fatalf("redirect is not to target service: %v", resp.Header.Get("Location"))
	}
}

func checkTargetSSOTicket(config *Config) func(testing.TB, *http.Response) {
	return func(t testing.TB, resp *http.Response) {
		u, err := url.Parse(resp.Header.Get("Location"))
		if err != nil {
			t.Fatalf("could not parse Location URL: %v", err)
		}
		tstr := u.Query().Get("t")
		nonce := u.Query().Get("n")

		// Validate the ticket in order to read it.
		v, err := newValidatorFromConfig(config)
		if err != nil {
			t.Fatalf("newValidatorFromConfig: %v", err)
		}
		ticket, err := v.Validate(tstr, nonce, "service.example.com/", nil)
		if err != nil {
			t.Fatalf("sso.Validate(%s): %v", tstr, err)
		}
		if n := len(ticket.Groups); n != 1 {
			t.Errorf("ticket has %d groups, expected 1", n)
		}
		if ticket.Groups[0] != "users" {
			t.Errorf("group is '%s', expected 'users'", ticket.Groups[0])
		}
	}
}

var usernameFieldRx = regexp.MustCompile(`<input[^>]*name="username"`)

func checkLoginPageURL(t testing.TB, resp *http.Response) {
	if resp.Request.URL.Path != "/login/" {
		t.Errorf("request path is not /login/ (%s)", resp.Request.URL.String())
	}
}

func checkLoginPasswordPage(t testing.TB, resp *http.Response) {
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("reading body: %v", err)
	}
	if !usernameFieldRx.Match(data) {
		t.Fatalf("not the password login page:\n%s", string(data))
	}
}

var otpFieldRx = regexp.MustCompile(`<input[^>]*name="otp"`)

func checkLoginOTPPageURL(t testing.TB, resp *http.Response) {
	if resp.Request.URL.Path != "/login/otp" {
		t.Errorf("request path is not /login/otp (%s)", resp.Request.URL.String())
	}
}

func checkLoginOTPPage(t testing.TB, resp *http.Response) {
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("reading body: %v", err)
	}
	if !otpFieldRx.Match(data) {
		t.Fatalf("not the OTP login page:\n%s", string(data))
	}
}

var authFailureRx = regexp.MustCompile(`<p\s*class="error">\s*Authentication failed`)

func checkAuthFailure(t testing.TB, resp *http.Response) {
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("reading body: %v", err)
	}
	if !authFailureRx.Match(data) {
		t.Fatalf("expected authentication failure, but no errors found:\n%s", string(data))
	}
}

func checkLogoutPage(t testing.TB, resp *http.Response) {
	if resp.Request.URL.Path != "/logout" {
		t.Errorf("request path is not /logout (%s)", resp.Request.URL.String())
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("reading body: %v", err)
	}

	if s := string(data); !strings.Contains(s, "Signing you out from all services") {
		t.Fatalf("not the logout page:\n%s", s)
	}
}

func extractSSOTicket(dest *string) func(testing.TB, *http.Response) {
	return func(t testing.TB, resp *http.Response) {
		u, err := url.Parse(resp.Header.Get("Location"))
		if err != nil {
			t.Fatalf("could not parse Location URL: %v", err)
		}
		*dest = u.Query().Get("t")
	}
}

func TestHTTP_Login(t *testing.T) {
	tmpdir, httpSrv, config := startTestHTTPServerWithConfig(t)
	defer os.RemoveAll(tmpdir)
	defer httpSrv.Close()

	c := newTestHTTPClient()

	// Simulate an authorization request from a service, expect to
	// see the login page.
	v := make(url.Values)
	v.Set("s", "service.example.com/")
	v.Set("d", "https://service.example.com/admin/")
	v.Set("n", "averysecretnonce")
	v.Set("g", "users")
	origPath := "/?" + v.Encode()
	doGet(t, c, httpSrv.URL+origPath, checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)

	// Attempt to login by submitting the form. We expect the
	// result to be a 302 redirect to the target service.
	v = make(url.Values)
	v.Set("username", "testuser")
	v.Set("password", "password")
	v.Set("r", origPath)
	doPostForm(t, c, httpSrv.URL+"/login/", v, checkRedirectToTargetService, checkTargetSSOTicket(config))
}

func TestHTTP_LoginOnSecondAttempt(t *testing.T) {
	tmpdir, httpSrv := startTestHTTPServer(t)
	defer os.RemoveAll(tmpdir)
	defer httpSrv.Close()

	c := newTestHTTPClient()

	// Simulate an authorization request from a service, expect to
	// see the login page.
	v := make(url.Values)
	v.Set("s", "service.example.com/")
	v.Set("d", "https://service.example.com/admin/")
	v.Set("n", "averysecretnonce")
	origPath := "/?" + v.Encode()
	doGet(t, c, httpSrv.URL+origPath, checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)

	// Attempt to login with wrong credentials.
	v = make(url.Values)
	v.Set("username", "testuser")
	v.Set("password", "badpassword")
	v.Set("r", origPath)
	doPostForm(t, c, httpSrv.URL+"/login/", v, checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)

	// Attempt to login by submitting the form. We expect the
	// result to be a 302 redirect to the target service.
	v = make(url.Values)
	v.Set("username", "testuser")
	v.Set("password", "password")
	v.Set("r", origPath)
	doPostForm(t, c, httpSrv.URL+"/login/", v, checkRedirectToTargetService)
}

func TestHTTP_LoginAndLogout(t *testing.T) {
	tmpdir, httpSrv := startTestHTTPServer(t)
	defer os.RemoveAll(tmpdir)
	defer httpSrv.Close()

	c := newTestHTTPClient()

	// Simulate an authorization request from a service, expect to
	// see the login page.
	v := make(url.Values)
	v.Set("s", "service.example.com/")
	v.Set("d", "https://service.example.com/admin/")
	v.Set("n", "averysecretnonce")
	origPath := "/?" + v.Encode()
	doGet(t, c, httpSrv.URL+origPath, checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)

	// Attempt to login by submitting the form. We expect the
	// result to be a 302 redirect to the target service.
	v = make(url.Values)
	v.Set("username", "testuser")
	v.Set("password", "password")
	v.Set("r", origPath)
	doPostForm(t, c, httpSrv.URL+"/login/", v, checkRedirectToTargetService)

	// Make a logout request.
	doGet(t, c, httpSrv.URL+"/logout", checkStatusOk, checkLogoutPage)

	// This new authorization request should send us to the login page.
	v = make(url.Values)
	v.Set("s", "service.example.com/")
	v.Set("d", "https://service.example.com/admin/")
	v.Set("n", "averysecretnonce")
	doGet(t, c, httpSrv.URL+"/?"+v.Encode(), checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)
}

func TestHTTP_LoginOTP(t *testing.T) {
	tmpdir, httpSrv := startTestHTTPServer(t)
	defer os.RemoveAll(tmpdir)
	defer httpSrv.Close()

	c := newTestHTTPClient()

	// Simulate an authorization request from a service, expect to
	// see the login page.
	v := make(url.Values)
	v.Set("s", "service.example.com/")
	v.Set("d", "https://service.example.com/admin/")
	v.Set("n", "averysecretnonce")
	origPath := "/?" + v.Encode()
	doGet(t, c, httpSrv.URL+origPath, checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)

	// Attempt to login by submitting the form. We should see the OTP page.
	v = make(url.Values)
	v.Set("username", "test2fa")
	v.Set("password", "password")
	v.Set("r", origPath)
	doPostForm(t, c, httpSrv.URL+"/login/", v, checkStatusOk, checkLoginOTPPageURL, checkLoginOTPPage)

	// Submit the correct OTP token. We expect the result to be a
	// 302 redirect to the target service.
	v = make(url.Values)
	v.Set("otp", "123456")
	v.Set("r", origPath)
	doPostForm(t, c, httpSrv.URL+"/login/otp", v, checkRedirectToTargetService)
}

func TestHTTP_LoginOTP_Fail(t *testing.T) {
	tmpdir, httpSrv := startTestHTTPServer(t)
	defer os.RemoveAll(tmpdir)
	defer httpSrv.Close()

	c := newTestHTTPClient()

	// Simulate an authorization request from a service, expect to
	// see the login page.
	v := make(url.Values)
	v.Set("s", "service.example.com/")
	v.Set("d", "https://service.example.com/admin/")
	v.Set("n", "averysecretnonce")
	origPath := "/?" + v.Encode()
	doGet(t, c, httpSrv.URL+origPath, checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)

	// Attempt to login by submitting the form. We should see the OTP page.
	v = make(url.Values)
	v.Set("username", "test2fa")
	v.Set("password", "password")
	v.Set("r", origPath)
	doPostForm(t, c, httpSrv.URL+"/login/", v, checkStatusOk, checkLoginOTPPageURL, checkLoginOTPPage)

	// Submit a bad OTP token, test for failure.
	v = make(url.Values)
	v.Set("otp", "000007")
	v.Set("r", origPath)
	doPostForm(t, c, httpSrv.URL+"/login/otp", v, checkAuthFailure)
}

func TestHTTP_LoginOTP_Intermediate404(t *testing.T) {
	// This test verifies that the session is not disrupted by a
	// request for a URL that does not exist during a 2FA login
	// workflow. The point is that the 404 should *not* Reset()
	// the session.
	tmpdir, httpSrv := startTestHTTPServer(t)
	defer os.RemoveAll(tmpdir)
	defer httpSrv.Close()

	c := newTestHTTPClient()

	// Simulate an authorization request from a service, expect to
	// see the login page.
	v := make(url.Values)
	v.Set("s", "service.example.com/")
	v.Set("d", "https://service.example.com/admin/")
	v.Set("n", "averysecretnonce")
	origPath := "/?" + v.Encode()
	doGet(t, c, httpSrv.URL+origPath, checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)

	// Attempt to login by submitting the form. We should see the OTP page.
	v = make(url.Values)
	v.Set("username", "test2fa")
	v.Set("password", "password")
	v.Set("r", origPath)
	doPostForm(t, c, httpSrv.URL+"/login/", v, checkStatusOk, checkLoginOTPPageURL, checkLoginOTPPage)

	// Make a request for a URL that does not exist, browsers
	// might do this for a number of reasons. Since there is an
	// explicit rule for this URL, the 404 will not disrupt the
	// request flow.
	doGet(t, c, httpSrv.URL+"/favicon.ico", checkStatusNotFound)

	// Submit the correct OTP token. We expect the result to be a
	// 302 redirect to the target service.
	v = make(url.Values)
	v.Set("otp", "123456")
	v.Set("r", origPath)
	doPostForm(t, c, httpSrv.URL+"/login/otp", v, checkRedirectToTargetService)
}

type fakeKeyStore struct {
	*httptest.Server
	values map[string]string
}

func createFakeKeyStore(t testing.TB, username, password string) *fakeKeyStore {
	values := make(map[string]string)
	h := http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if req.URL.Path != "/api/open" {
			http.NotFound(w, req)
			return
		}
		var openReq keystore.OpenRequest
		if err := json.NewDecoder(req.Body).Decode(&openReq); err != nil {
			t.Errorf("bad JSON body: %v", err)
			return
		}
		values[openReq.Username] = openReq.Password
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, "{}") // nolint
	})
	return &fakeKeyStore{
		Server: httptest.NewServer(h),
		values: values,
	}
}

func TestHTTP_LoginWithKeyStore(t *testing.T) {
	tmpdir, httpSrv, ks := startTestHTTPServerWithKeyStore(t)
	defer os.RemoveAll(tmpdir)
	defer httpSrv.Close()

	c := newTestHTTPClient()

	// Simulate an authorization request from a service, expect to
	// see the login page.
	v := make(url.Values)
	v.Set("s", "service.example.com/")
	v.Set("d", "https://service.example.com/admin/")
	v.Set("n", "averysecretnonce")
	origPath := "/?" + v.Encode()
	doGet(t, c, httpSrv.URL+origPath, checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)

	// Attempt to login by submitting the form. We expect the
	// result to be a 302 redirect to the target service.
	v = make(url.Values)
	v.Set("username", "testuser")
	v.Set("password", "password")
	v.Set("r", origPath)
	doPostForm(t, c, httpSrv.URL+"/login/", v, checkRedirectToTargetService)

	// Verify that the keystore has been called.
	if v := ks.values["testuser"]; v != "password" {
		t.Fatalf("keystore not called as expected: ks_values=%+v", ks.values)
	}
}

func TestHTTP_CORS(t *testing.T) {
	tmpdir, httpSrv := startTestHTTPServer(t)
	defer os.RemoveAll(tmpdir)
	defer httpSrv.Close()

	c := newTestHTTPClient()

	// To test a CORS preflight request we have to login first.
	// Simulate an authorization request from a service, expect to
	// see the login page.
	v := make(url.Values)
	v.Set("s", "service.example.com/")
	v.Set("d", "https://service.example.com/admin/")
	v.Set("n", "averysecretnonce")
	origPath := "/?" + v.Encode()
	doGet(t, c, httpSrv.URL+origPath, checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)

	// Attempt to login by submitting the form. We expect the
	// result to be a 302 redirect to the target service.
	v = make(url.Values)
	v.Set("username", "testuser")
	v.Set("password", "password")
	v.Set("r", origPath)
	doPostForm(t, c, httpSrv.URL+"/login/", v, checkRedirectToTargetService)

	// Simulate a CORS preflight request.
	v = make(url.Values)
	v.Set("s", "service.example.com/")
	v.Set("d", "https://service.example.com/admin/")
	v.Set("n", "averysecretnonce")
	req, err := http.NewRequest("OPTIONS", httpSrv.URL+"/?"+v.Encode(), nil)
	if err != nil {
		t.Fatalf("NewRequest(): %v", err)
	}
	req.Header.Set("Origin", "https://origin.example.com")
	req.Header.Set("Access-Control-Request-Method", "GET")
	resp, err := c.Do(req)
	if err != nil {
		t.Fatalf("http request error: %v", err)
	}
	defer resp.Body.Close()
	checkStatusOk(t, resp)
	if s := resp.Header.Get("Access-Control-Allow-Origin"); s != "https://origin.example.com" {
		t.Fatalf("Bad Access-Control-Allow-Origin returned to OPTIONS request: %s", s)
	}
}

func TestHTTP_LoginAndExchange(t *testing.T) {
	tmpdir, httpSrv := startTestHTTPServer(t)
	defer os.RemoveAll(tmpdir)
	defer httpSrv.Close()

	c := newTestHTTPClient()

	// Simulate an authorization request from a service, expect to
	// see the login page.
	v := make(url.Values)
	v.Set("s", "service.example.com/")
	v.Set("d", "https://service.example.com/admin/")
	v.Set("n", "averysecretnonce")
	origPath := "/?" + v.Encode()
	doGet(t, c, httpSrv.URL+origPath, checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)

	// Attempt to login by submitting the form. We expect the
	// result to be a 302 redirect to the target service.
	v = make(url.Values)
	v.Set("username", "testuser")
	v.Set("password", "password")
	v.Set("r", origPath)
	var ssoTkt string
	doPostForm(t, c, httpSrv.URL+"/login/", v, checkRedirectToTargetService, extractSSOTicket(&ssoTkt))

	// Make an exchange request for a new service.
	v = make(url.Values)
	v.Set("cur_tkt", ssoTkt)
	v.Set("cur_svc", "service.example.com/")
	v.Set("cur_nonce", "averysecretnonce")
	v.Set("new_svc", "service2.example.com/")
	v.Set("new_nonce", "anothernonce")
	doPostForm(t, c, httpSrv.URL+"/exchange", v, checkStatusOk)

	// Make an exchange request for a forbidden service.
	v = make(url.Values)
	v.Set("cur_tkt", ssoTkt)
	v.Set("cur_svc", "service.example.com/")
	v.Set("cur_nonce", "averysecretnonce")
	v.Set("new_svc", "service3.example.com/")
	v.Set("new_nonce", "anothernonce")
	doPostForm(t, c, httpSrv.URL+"/exchange", v, checkStatusForbidden)
}
