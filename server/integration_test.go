package server

import (
	"html"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"testing"

	"git.autistici.org/id/go-sso/httpsso"
	"github.com/gorilla/securecookie"
)

// Create a SSO-wrapped service.
func createTestProtectedService(t testing.TB, serverURL, tmpdir string) *httptest.Server {
	ssoPubKey, err := ioutil.ReadFile(filepath.Join(tmpdir, "public"))
	if err != nil {
		t.Fatalf("oops, can't read sso public key: %v", err)
	}

	w, err := httpsso.NewSSOWrapper(
		serverURL,
		ssoPubKey,
		"example.com",
		securecookie.GenerateRandomKey(64),
		securecookie.GenerateRandomKey(32),
		0,
	)
	if err != nil {
		t.Fatalf("NewSSOWrapper(): %v", err)
	}

	h := w.Wrap(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.Write([]byte("OK")) // nolint
	}), "service.example.com/", nil)

	return httptest.NewTLSServer(h)
}

func startTestHTTPServerAndApp(t testing.TB) (string, *httptest.Server, *httptest.Server) {
	tmpdir, _ := ioutil.TempDir("", "")
	config := testConfig(t, tmpdir, "")
	srv := createTestHTTPServer(t, config)
	app := createTestProtectedService(t, "https://login.example.com/", tmpdir)
	return tmpdir, srv, app
}

func startTestHTTPServerWithPrefixAndApp(t testing.TB) (string, *httptest.Server, *httptest.Server) {
	tmpdir, _ := ioutil.TempDir("", "")
	config := testConfig(t, tmpdir, "")
	config.URLPrefix = "/sso"
	srv := createTestHTTPServer(t, config)
	app := createTestProtectedService(t, "https://login.example.com/sso/", tmpdir)
	return tmpdir, srv, app
}

func checkLoginPageURLWithPrefix(t testing.TB, resp *http.Response) {
	if resp.Request.URL.Path != "/sso/login/" {
		t.Errorf("request path is not /sso/login/ (%s)", resp.Request.URL.String())
	}
}

func checkIsProtectedService(t testing.TB, resp *http.Response) {
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("reading body: %v", err)
	}
	if s := string(data); s != "OK" {
		t.Fatalf("not the target application, response body='%s'", s)
	}
}

func checkLogoutPageHasLinks(t testing.TB, resp *http.Response) {
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("reading body: %v", err)
	}
	logoutURL := "https://service.example.com/sso_logout"
	if sdata := string(data); !strings.Contains(sdata, logoutURL) {
		t.Fatalf("service logout URL not found in logout page:\n%s", sdata)
	}
}

func addrFromURL(s string) string {
	u, _ := url.Parse(s)
	if !strings.Contains(u.Host, ":") {
		return u.Host + ":443"
	}
	return u.Host
}

// The integration test spins up an actual service and verifies the
// interaction between it and the login application, using DNS-level
// overrides to ensure proper name validation.
func TestIntegration(t *testing.T) {
	tmpdir, srv, app := startTestHTTPServerAndApp(t)
	defer os.RemoveAll(tmpdir)
	defer srv.Close()
	defer app.Close()

	b := newBrowser(map[string]string{
		"login.example.com:443":   addrFromURL(srv.URL),
		"service.example.com:443": addrFromURL(app.URL),
	})

	b.get(t, "https://service.example.com/", checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)

	v := make(url.Values)
	v.Set("username", "testuser")
	v.Set("password", "password")
	b.post(t, v, checkStatusOk, checkIsProtectedService)

	// Now attempt to logout, and verify that we can't access the service anymore.
	b.get(t, "https://login.example.com/logout", checkStatusOk, checkLogoutPageHasLinks)
	b.get(t, "https://service.example.com/sso_logout", checkStatusOk)
	b.get(t, "https://service.example.com/", checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)
}

func TestIntegration_2FA(t *testing.T) {
	tmpdir, srv, app := startTestHTTPServerAndApp(t)
	defer os.RemoveAll(tmpdir)
	defer srv.Close()
	defer app.Close()

	b := newBrowser(map[string]string{
		"login.example.com:443":   addrFromURL(srv.URL),
		"service.example.com:443": addrFromURL(app.URL),
	})

	b.get(t, "https://service.example.com/", checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)

	v := make(url.Values)
	v.Set("username", "test2fa")
	v.Set("password", "password")
	b.post(t, v, checkStatusOk, checkLoginOTPPage)

	v = make(url.Values)
	v.Set("otp", "123456")
	b.post(t, v, checkStatusOk, checkIsProtectedService)

	// Now attempt to logout, and verify that we can't access the service anymore.
	b.get(t, "https://login.example.com/logout", checkStatusOk, checkLogoutPageHasLinks)
	b.get(t, "https://service.example.com/sso_logout", checkStatusOk)
	b.get(t, "https://service.example.com/", checkStatusOk, checkLoginPageURL, checkLoginPasswordPage)
}

// Same test as above, but the server application has a URL prefix.
func TestIntegration_2FA_WithURLPrefix(t *testing.T) {
	tmpdir, srv, app := startTestHTTPServerWithPrefixAndApp(t)
	defer os.RemoveAll(tmpdir)
	defer srv.Close()
	defer app.Close()

	b := newBrowser(map[string]string{
		"login.example.com:443":   addrFromURL(srv.URL),
		"service.example.com:443": addrFromURL(app.URL),
	})

	b.get(t, "https://service.example.com/", checkStatusOk, checkLoginPageURLWithPrefix, checkLoginPasswordPage)

	v := make(url.Values)
	v.Set("username", "test2fa")
	v.Set("password", "password")
	b.post(t, v, checkStatusOk, checkLoginOTPPage)

	v = make(url.Values)
	v.Set("otp", "123456")
	b.post(t, v, checkStatusOk, checkIsProtectedService)

	// Now attempt to logout, and verify that we can't access the service anymore.
	b.get(t, "https://login.example.com/sso/logout", checkStatusOk, checkLogoutPageHasLinks)
	b.get(t, "https://service.example.com/sso_logout", checkStatusOk)
	b.get(t, "https://service.example.com/", checkStatusOk, checkLoginPageURLWithPrefix, checkLoginPasswordPage)
}

// Minimal "browser" equivalent that maintains cookie and page state
// and can extract form parameters from the current page, simulating
// what an actual browser would do when following the login workflow.
//
// By using this we don't have to make assumptions about the generated
// URLs, and we can verify that they are consistent.
//
type browser struct {
	c *http.Client

	url          url.URL
	formAction   string
	hiddenFields map[string]string
}

func newBrowser(dnsOverrides map[string]string) *browser {
	c := makeHTTPClient(dnsOverrides, true)
	return &browser{c: c}
}

// TODO: This is bad and should be replaced by something more strict
// such as PuerkitoBio/query or similar high-level HTML parsers.
var (
	formActionRx  = regexp.MustCompile(`<form\s+[^>]*action="([^"]*)"`)
	inputHiddenRx = regexp.MustCompile(`<input\s+([^>]*type="hidden"[^>]*)>`)
	inputNameRx   = regexp.MustCompile(`name="([^"]*)"`)
	inputValueRx  = regexp.MustCompile(`value="([^"]*)"`)
)

func (b *browser) updateCurrentPage(t testing.TB, resp *http.Response) {
	b.url = *resp.Request.URL

	data, _ := ioutil.ReadAll(resp.Body)

	if m := formActionRx.FindSubmatch(data); m != nil {
		parsed, _ := url.Parse(html.UnescapeString(string(m[1])))
		abs := b.url.ResolveReference(parsed)
		b.formAction = abs.String()
	} else {
		b.formAction = ""
	}

	fields := make(map[string]string)
	for _, m := range inputHiddenRx.FindAllSubmatch(data, -1) {
		namem := inputNameRx.FindSubmatch(m[1])
		valuem := inputValueRx.FindSubmatch(m[1])
		if namem == nil || valuem == nil {
			continue
		}
		fields[string(namem[1])] = html.UnescapeString(string(valuem[1]))
	}
	b.hiddenFields = fields
}

func (b *browser) get(t testing.TB, uri string, checkResponse ...func(testing.TB, *http.Response)) {
	checkResponse = append(checkResponse, b.updateCurrentPage)
	doGet(t, b.c, uri, checkResponse...)
}

func (b *browser) post(t testing.TB, v url.Values, checkResponse ...func(testing.TB, *http.Response)) {
	if b.formAction == "" {
		t.Fatalf("browser hasn't detected a form to submit in the current page (%s)", b.url.String())
	}

	for key, value := range b.hiddenFields {
		v.Set(key, value)
	}

	checkResponse = append(checkResponse, b.updateCurrentPage)
	doPostForm(t, b.c, b.formAction, v, checkResponse...)
}
