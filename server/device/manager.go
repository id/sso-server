package device

import (
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"

	"git.autistici.org/id/sso-server/common"
	"git.autistici.org/id/usermetadb"
	"github.com/gorilla/securecookie"
	"github.com/mssola/user_agent"
)

const (
	deviceIDBytes        = 8
	deviceIDCookieName   = "_dev"
	deviceIDCookieMaxAge = 10 * 365 * 86400
)

var errZoneNotFound = errors.New("no zone found")

type zoneMapper interface {
	ResolveZone(net.IP) (string, error)
}

// DeviceID is an opaque random device identifier.
type DeviceID []byte

func (d DeviceID) String() string {
	return hex.EncodeToString(d)
}

func randomDeviceID() DeviceID {
	return DeviceID(common.RandomBytes(deviceIDBytes))
}

// Manager can provide DeviceInfo entries for incoming HTTP requests.
type Manager struct {
	sc          *securecookie.SecureCookie
	urlPrefix   string
	zoneMappers []zoneMapper
}

// ZoneMapSpec describes ip -> zone lookup maps.
type ZoneMapSpec struct {
	Type           string   `yaml:"type"`
	GeoIPDataFiles []string `yaml:"geoip_data_files"`
	Path           string   `yaml:"path"`
	Value          string   `yaml:"value"`
}

// Config stores options for the device info manager.
type Config struct {
	AuthKey  string         `yaml:"auth_key"`
	ZoneMaps []*ZoneMapSpec `yaml:"zone_maps"`
}

// New returns a new Manager with the given configuration.
func New(config *Config, urlPrefix string) (*Manager, error) {
	if config == nil {
		config = new(Config)
	}

	// Instantiate all our IP -> zone lookup tables. Reminder that
	// GeoIP should go last because it always has an answer.
	var zoneMappers []zoneMapper
	for _, spec := range config.ZoneMaps {
		m, err := newZoneMapper(spec)
		if err != nil {
			return nil, fmt.Errorf("error in zone_maps: %w", err)
		}
		zoneMappers = append(zoneMappers, m)
	}

	// This should only happen in tests.
	if config.AuthKey == "" {
		log.Printf("Warning: device_manager.auth_key unset, generating temporary random secrets")
		config.AuthKey = string(securecookie.GenerateRandomKey(64))
	}

	sc := securecookie.New([]byte(config.AuthKey), nil)
	sc.MaxAge(deviceIDCookieMaxAge)
	sc.SetSerializer(securecookie.NopEncoder{})

	return &Manager{
		sc:          sc,
		urlPrefix:   strings.TrimRight(urlPrefix, "/"),
		zoneMappers: zoneMappers,
	}, nil
}

// GetDeviceInfoFromRequest will retrieve or create a DeviceInfo
// object for the given request. It will always return a valid object.
// The ResponseWriter is needed to store the unique ID on the client
// when a new device info object is created.
func (m *Manager) GetDeviceInfoFromRequest(w http.ResponseWriter, req *http.Request) *usermetadb.DeviceInfo {
	devID, ok := m.getDeviceCookie(req)
	if !ok || len(devID) == 0 {
		// Generate a new Device ID and save it on the client.
		devID = randomDeviceID()
		if err := m.setDeviceCookie(w, devID); err != nil {
			// This is likely a misconfiguration issue, so
			// we want to know about it.
			log.Printf("error saving device manager session: %v", err)
		}
	}

	uaStr := req.UserAgent()
	ua := user_agent.New(uaStr)
	browser, _ := ua.Browser()
	d := usermetadb.DeviceInfo{
		ID:        devID.String(),
		UserAgent: uaStr,
		Mobile:    ua.Mobile(),
		OS:        ua.OS(),
		Browser:   browser,
	}

	ip := getIPFromRequest(req)
	switch {
	case strings.HasSuffix(req.Host, ".onion"):
		// Special check for .onion HTTP hosts - sets the zone to
		// "onion" and skips the IP-based checks.
		d.RemoteZone = "onion"

	case ip != nil:
		// If we have an IP address, we can add more information.
		d.RemoteAddr = ip.String()

		// Zone lookup.
		for _, zm := range m.zoneMappers {
			if zone, err := zm.ResolveZone(ip); err == nil {
				d.RemoteZone = zone
				break
			}
		}
	}

	return &d
}

func (m *Manager) getDeviceCookie(r *http.Request) (DeviceID, bool) {
	if cookie, err := r.Cookie(deviceIDCookieName); err == nil {
		var value []byte
		if err = m.sc.Decode(deviceIDCookieName, cookie.Value, &value); err == nil {
			return DeviceID(value), true
		}
	}
	return nil, false
}

func (m *Manager) setDeviceCookie(w http.ResponseWriter, value DeviceID) error {
	encoded, err := m.sc.Encode(deviceIDCookieName, []byte(value))
	if err != nil {
		return err
	}
	cookie := &http.Cookie{
		Name:     deviceIDCookieName,
		Value:    encoded,
		Path:     m.urlPrefix + "/",
		Secure:   true,
		HttpOnly: true,
		MaxAge:   deviceIDCookieMaxAge,
		SameSite: http.SameSiteStrictMode,
	}
	http.SetCookie(w, cookie)
	return nil
}

func getIPFromRequest(req *http.Request) net.IP {
	// Parse the RemoteAddr Request field, for starters.
	host, _, err := net.SplitHostPort(req.RemoteAddr)
	if err != nil {
		host = req.RemoteAddr
	}
	return net.ParseIP(host)
}

func newZoneMapper(spec *ZoneMapSpec) (zoneMapper, error) {
	switch spec.Type {
	case "geoip":
		return newGeoIP(spec.GeoIPDataFiles)
	case "ipset":
		return newIPSetFileZoneMapper(spec.Path, spec.Value)
	default:
		return nil, fmt.Errorf("unknown zone map type '%s'", spec.Type)
	}
}
