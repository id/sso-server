package device

import (
	"bufio"
	"errors"
	"io"
	"log"
	"net"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/yl2chen/cidranger"
)

// ipset is a reloadable threadsafe container for a cidranger.Ranger.
type ipset struct {
	value string
	mx    sync.Mutex
	cur   cidranger.Ranger
}

func newIPSet(value string) *ipset {
	return &ipset{value: value}
}

func (s *ipset) contains(ip net.IP) bool {
	s.mx.Lock()
	defer s.mx.Unlock()

	if s.cur == nil {
		return false
	}
	ok, _ := s.cur.Contains(ip)
	return ok
}

func (s *ipset) update(set cidranger.Ranger) {
	s.mx.Lock()
	s.cur = set
	s.mx.Unlock()
}

func (c *ipset) ResolveZone(ip net.IP) (string, error) {
	if c.contains(ip) {
		return c.value, nil
	}
	return "", errZoneNotFound
}

type parsedIP struct {
	ipnet net.IPNet
}

func (p *parsedIP) Network() net.IPNet {
	return p.ipnet
}

func parseIP(s string) (*parsedIP, error) {
	if !strings.Contains(s, "/") {
		if strings.Contains(s, ":") {
			s += "/128"
		} else {
			s += "/32"
		}
	}
	_, ipnet, err := net.ParseCIDR(s)
	if err != nil {
		return nil, err
	}
	return &parsedIP{*ipnet}, nil
}

func parseRanger(r io.Reader) (cidranger.Ranger, error) {
	set := cidranger.NewPCTrieRanger()

	scanner := bufio.NewScanner(bufio.NewReader(r))
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 || line[0] == '#' {
			continue
		}
		if ip, err := parseIP(line); err == nil {
			set.Insert(ip) // nolint: errcheck
		}
	}

	return set, scanner.Err()
}

func rangerFromFile(path string) (cidranger.Ranger, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return parseRanger(f)
}

func isSameFile(a, b os.FileInfo) bool {
	switch {
	case a == nil && b == nil:
		return true
	case a == nil || b == nil:
		return false
	}
	return (a.Size() == b.Size() && a.ModTime() == b.ModTime())
}

// Call fn(path) right away, and whenever path has changed. Closes the
// given channel after the initial load.
func watchFile(path string, fn func(string) error, ready chan struct{}) {
	curInfo, err := os.Stat(path)
	if err == nil {
		if err := fn(path); err != nil {
			log.Printf("error loading %s: %v", path, err)
		}
	}
	close(ready)

	tick := time.NewTicker(10 * time.Second)
	for range tick.C {
		info, err := os.Stat(path)
		if err != nil {
			continue
		}
		if isSameFile(info, curInfo) {
			continue
		}

		curInfo = info
		if err := fn(path); err != nil {
			log.Printf("error loading %s: %v", path, err)
		}
	}
}

func newIPSetFileZoneMapper(path, value string) (*ipset, error) {
	if path == "" {
		return nil, errors.New("no path specified for ipset")
	}
	if value == "" {
		return nil, errors.New("no value specified for ipset")
	}

	set := newIPSet(value)

	ready := make(chan struct{})
	go watchFile(path, func(path string) error {
		data, err := rangerFromFile(path)
		if err != nil {
			return err
		}

		set.update(data)
		return nil
	}, ready)

	<-ready
	return set, nil
}
