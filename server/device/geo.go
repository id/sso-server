package device

import (
	"net"
	"os"

	"github.com/oschwald/maxminddb-golang"
)

var defaultGeoIPPaths = []string{
	"/var/lib/GeoIP/GeoLite2-Country.mmdb",
}

type geoIPDb struct {
	readers []*maxminddb.Reader
}

func newGeoIP(paths []string) (*geoIPDb, error) {
	if len(paths) == 0 {
		paths = defaultGeoIPPaths
	}

	db := new(geoIPDb)
	for _, path := range paths {
		// Only attempt to load zone files if they actually exist.
		if _, err := os.Stat(path); os.IsNotExist(err) {
			continue
		}
		geodb, err := maxminddb.Open(path)
		if err != nil {
			return nil, err
		}
		db.readers = append(db.readers, geodb)
	}

	return db, nil
}

func (db *geoIPDb) ResolveZone(ip net.IP) (string, error) {
	// Only look up a single attribute (country).
	var record struct {
		Country struct {
			ISOCode string `maxminddb:"iso_code"`
		} `maxminddb:"country"`
	}

	for _, r := range db.readers {
		if err := r.Lookup(ip, &record); err == nil {
			return record.Country.ISOCode, nil
		}
	}
	return "", errZoneNotFound
}
