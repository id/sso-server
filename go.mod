module git.autistici.org/id/sso-server

go 1.21.0

toolchain go1.22.1

require (
	git.autistici.org/ai3/go-common v0.0.0-20241017171051-880a2c5ae7f4
	git.autistici.org/id/auth v0.0.0-20241017204112-73812019f8b2
	git.autistici.org/id/go-sso v0.0.0-20241017184626-0e26b5e055dc
	git.autistici.org/id/keystore v0.0.0-20230901162242-63f23c4799e9
	git.autistici.org/id/usermetadb v0.0.0-20241017171915-b5c24a0ff9b7
	github.com/crewjam/saml v0.4.14
	github.com/elazarl/go-bindata-assetfs v1.0.1
	github.com/go-webauthn/webauthn v0.10.2
	github.com/gorilla/csrf v1.7.2
	github.com/gorilla/mux v1.8.1
	github.com/gorilla/securecookie v1.1.2
	github.com/mssola/user_agent v0.6.0
	github.com/oschwald/maxminddb-golang v1.13.1
	github.com/prometheus/client_golang v1.20.5
	github.com/rs/cors v1.11.1
	github.com/yl2chen/cidranger v1.0.2
	go.opentelemetry.io/otel v1.10.0
	go.opentelemetry.io/otel/trace v1.10.0
	golang.org/x/crypto v0.28.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/NYTimes/gziphandler v1.1.1 // indirect
	github.com/beevik/etree v1.1.0 // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cenkalti/backoff/v4 v4.3.0 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/coreos/go-systemd/v22 v22.5.0 // indirect
	github.com/felixge/httpsnoop v1.0.3 // indirect
	github.com/fxamacker/cbor/v2 v2.7.0 // indirect
	github.com/go-logr/logr v1.2.3 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/go-webauthn/x v0.1.9 // indirect
	github.com/gofrs/flock v0.12.1 // indirect
	github.com/golang-jwt/jwt/v5 v5.2.1 // indirect
	github.com/google/go-tpm v0.9.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/klauspost/compress v1.17.9 // indirect
	github.com/mattermost/xml-roundtrip-validator v0.1.0 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/munnerz/goautoneg v0.0.0-20191010083416-a7dc8b61c822 // indirect
	github.com/openzipkin/zipkin-go v0.4.0 // indirect
	github.com/prometheus/client_model v0.6.1 // indirect
	github.com/prometheus/common v0.55.0 // indirect
	github.com/prometheus/procfs v0.15.1 // indirect
	github.com/russellhaering/goxmldsig v1.3.0 // indirect
	github.com/x448/float16 v0.8.4 // indirect
	go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp v0.34.0 // indirect
	go.opentelemetry.io/contrib/propagators/b3 v1.9.0 // indirect
	go.opentelemetry.io/otel/exporters/zipkin v1.9.0 // indirect
	go.opentelemetry.io/otel/metric v0.31.0 // indirect
	go.opentelemetry.io/otel/sdk v1.10.0 // indirect
	golang.org/x/sync v0.8.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
	google.golang.org/protobuf v1.34.2 // indirect
)
