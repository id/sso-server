package proxy

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	"github.com/gorilla/mux"

	"git.autistici.org/id/go-sso/httpsso"
	"git.autistici.org/id/sso-server/common"
)

// TTL for SSO sessions on the proxy.
var proxyAuthTTL = 1 * time.Hour

// RNG for the random backend selector.
var rnd = common.WeakRandomSource()

// Backend defines a single-host HTTP proxy to a set of upstream
// backends.
type Backend struct {
	Host            string                      `yaml:"host"`
	Upstream        []string                    `yaml:"upstream"`
	ServerName      string                      `yaml:"tls_server_name"`
	ClientTLSConfig *clientutil.TLSClientConfig `yaml:"client_tls"`

	AllowedGroups []string `yaml:"allowed_groups"`
}

func (b *Backend) newHandler(ssow *httpsso.SSOWrapper) (http.Handler, error) {
	// Setup upstream connections.
	if len(b.Upstream) < 1 {
		return nil, errors.New("no backends specified")
	}

	u := &url.URL{Scheme: "http", Host: b.Host}
	if b.ClientTLSConfig != nil {
		u.Scheme = "https"
	}

	proxy := httputil.NewSingleHostReverseProxy(u)

	var tlsConfig *tls.Config
	if b.ClientTLSConfig != nil {
		var err error
		tlsConfig, err = b.ClientTLSConfig.TLSConfig()
		if err != nil {
			return nil, err
		}

		// By setting the ServerName on the tls.Config, we
		// hope to decouple TLS certificate verification from
		// the details of the HTTP Host header included in the
		// request, so that the transport layer will work
		// regardless of the HTTP request details.
		tlsConfig.ServerName = b.ServerName
	}
	proxy.Transport = &http.Transport{
		TLSClientConfig: tlsConfig,
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			// The simplest possible backend
			// load-balancer: we just pick a random
			// upstream target to connect to every time.
			var d net.Dialer
			return d.DialContext(ctx, network, b.Upstream[rnd.Intn(len(b.Upstream))])
		},

		// Parameters match those of net/http.DefaultTransport.
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}

	h := ssow.Wrap(proxy, b.Host+"/", b.AllowedGroups)
	return h, nil
}

// Config for the proxy.
type Config struct {
	SessionAuthKey common.SessionAuthenticationKey `yaml:"session_auth_key"`
	SessionEncKey  common.SessionEncryptionKey     `yaml:"session_enc_key"`

	SSOLoginServerURL string `yaml:"sso_server_url"`
	SSOPublicKeyFile  string `yaml:"sso_public_key_file"`
	SSODomain         string `yaml:"sso_domain"`

	Backends []*Backend `yaml:"backends"`
}

// Sanity checks for the configuration.
func (c *Config) check() error {
	if len(c.SessionAuthKey) == 0 {
		return errors.New("session_auth_key is empty")
	}
	if len(c.SessionEncKey) == 0 {
		return errors.New("session_enc_key is empty")
	}
	if c.SSOLoginServerURL == "" {
		return errors.New("sso_server_url is empty")
	}
	if c.SSODomain == "" {
		return errors.New("sso_domain is empty")
	}
	return nil
}

// NewProxy builds a SSO-protected multi-host handler with the
// specified configuration.
func NewProxy(config *Config) (http.Handler, error) {
	if err := config.check(); err != nil {
		return nil, err
	}

	pkey, err := ioutil.ReadFile(config.SSOPublicKeyFile)
	if err != nil {
		return nil, err
	}

	w, err := httpsso.NewSSOWrapper(
		config.SSOLoginServerURL,
		pkey,
		config.SSODomain,
		[]byte(config.SessionAuthKey),
		[]byte(config.SessionEncKey),
		proxyAuthTTL,
	)
	if err != nil {
		return nil, err
	}

	r := mux.NewRouter()
	for _, b := range config.Backends {
		h, err := b.newHandler(w)
		if err != nil {
			return nil, fmt.Errorf("error for host %s: %v", b.Host, err)
		}
		r.Host(b.Host).Handler(h)
	}
	return r, nil
}
