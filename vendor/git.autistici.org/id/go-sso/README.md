sso
===

Native Golang implementation of the [ai/sso](https://git.autistici.org/ai/sso)
single sign-on mechanism.

This repository also used to include the login service, which has
now been split off into its own repository at [id/sso-server](https://git.autistici.org/id/sso-server).

