package httpsso

import (
	"context"
	"crypto/rand"
	"encoding/gob"
	"encoding/hex"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"git.autistici.org/id/go-sso"
	"github.com/gorilla/securecookie"
)

const (
	ssoCookieName   = "sso"
	nonceCookieName = "sso_n"
)

type authSession struct {
	Auth     bool
	Username string
	Groups   []string
}

type authSessionKeyType int

const authSessionKey authSessionKeyType = 0

func getCurrentAuthSession(req *http.Request) *authSession {
	if s, ok := req.Context().Value(authSessionKey).(*authSession); ok {
		return s
	}
	return nil
}

// Authenticated returns true if the user is successfully
// authenticated, in the call trace following SSOWrapper.Wrap.
func Authenticated(req *http.Request) bool {
	if s := getCurrentAuthSession(req); s != nil {
		return s.Auth
	}
	return false
}

// Username of the currently authenticated user.
func Username(req *http.Request) string {
	if s := getCurrentAuthSession(req); s != nil && s.Auth {
		return s.Username
	}
	return ""
}

// Groups returns the group list for the currently authenticated user.
func Groups(req *http.Request) []string {
	if s := getCurrentAuthSession(req); s != nil && s.Auth {
		return s.Groups
	}
	return nil
}

var defaultAuthSessionTTL = 1 * time.Hour

func init() {
	gob.Register(&authSession{})
}

// SSOWrapper protects http handlers with single-sign-on authentication.
type SSOWrapper struct {
	v            sso.Validator
	sc           *securecookie.SecureCookie
	serverURL    string
	serverOrigin string

	EnableCORS bool
	SameSite   http.SameSite
}

// NewSSOWrapper returns a new SSOWrapper that will authenticate users
// on the specified login service.
func NewSSOWrapper(serverURL string, pkey []byte, domain string, sessionAuthKey, sessionEncKey []byte, ttl time.Duration) (*SSOWrapper, error) {
	v, err := sso.NewValidator(pkey, domain)
	if err != nil {
		return nil, err
	}

	if ttl == 0 {
		ttl = defaultAuthSessionTTL
	}
	sc := securecookie.New(sessionAuthKey, sessionEncKey).MaxAge(int(ttl.Seconds()))

	return &SSOWrapper{
		v:            v,
		sc:           sc,
		serverURL:    serverURL,
		serverOrigin: originFromURL(serverURL),
		EnableCORS:   true,
		SameSite:     http.SameSiteNoneMode,
	}, nil
}

// Wrap a http.Handler with authentication and access control.
// Currently only a simple form of group-based ACLs is supported.
func (s *SSOWrapper) Wrap(h http.Handler, service string, groups []string) http.Handler {
	svcPath := pathFromService(service)
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		switch strings.TrimPrefix(req.URL.Path, svcPath) {
		case "sso_login":
			s.handleLogin(w, req, service, groups)

		case "sso_logout":
			s.handleLogout(w, req)

		default:
			var auth authSession
			if cookie, err := req.Cookie(ssoCookieName); err == nil {
				s.sc.Decode(ssoCookieName, cookie.Value, &auth) // nolint
			}

			if auth.Auth {
				req.Header.Set("X-Authenticated-User", auth.Username)
				req = req.WithContext(context.WithValue(req.Context(), authSessionKey, &auth))
				h.ServeHTTP(w, req)
				return
			}

			s.redirectToLogin(w, req, service, groups)
		}
	})
}

func (s *SSOWrapper) handleLogin(w http.ResponseWriter, req *http.Request, service string, groups []string) {
	t := req.FormValue("t")
	d := req.FormValue("d")

	// Pop the nonce from the cookies.
	cookie, err := req.Cookie(nonceCookieName)
	if err != nil {
		log.Printf("got login request without nonce")
		http.Error(w, "Missing nonce", http.StatusBadRequest)
		return
	}
	nonce := cookie.Value
	cookie.MaxAge = -1
	cookie.Value = ""
	cookie.SameSite = s.SameSite
	cookie.Secure = true
	http.SetCookie(w, cookie)

	tkt, err := s.v.Validate(t, nonce, service, groups)
	if err != nil {
		log.Printf("validation error for token %s: %v", t, err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Authenticate the user.
	auth := authSession{
		Auth:     true,
		Username: tkt.User,
		Groups:   tkt.Groups,
	}
	encoded, err := s.sc.Encode(ssoCookieName, &auth)
	if err != nil {
		log.Printf("error saving SSO session: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name:     ssoCookieName,
		Value:    encoded,
		Path:     pathFromService(service),
		Secure:   true,
		HttpOnly: true,
		SameSite: s.SameSite,
	})

	s.redirectWithCORS(w, req, d)
}

func (s *SSOWrapper) handleLogout(w http.ResponseWriter, req *http.Request) {
	// Delete the session cookie, if present.
	if cookie, err := req.Cookie(ssoCookieName); err == nil {
		cookie.MaxAge = -1
		cookie.Value = ""
		http.SetCookie(w, cookie)
	}

	w.Header().Set("Content-Type", "text/plain")
	if s.serverOrigin != "" {
		w.Header().Set("Access-Control-Allow-Origin", s.serverOrigin)
		w.Header().Set("Access-Control-Allow-Credentials", "true")
	}
	io.WriteString(w, "OK") // nolint
}

// Redirect to the SSO server.
func (s *SSOWrapper) redirectToLogin(w http.ResponseWriter, req *http.Request, service string, groups []string) {
	// Generate a random nonce and store it in a cookie.
	nonce := makeUniqueNonce()
	http.SetCookie(w, &http.Cookie{
		Name:     nonceCookieName,
		Value:    nonce,
		Path:     pathFromService(service) + "sso_login",
		Secure:   true,
		HttpOnly: true,
		SameSite: s.SameSite,
	})

	v := make(url.Values)
	v.Set("s", service)
	v.Set("d", getFullURL(req, "https").String())
	v.Set("n", nonce)
	v.Set("g", strings.Join(groups, ","))
	loginURL := s.serverURL + "?" + v.Encode()
	s.redirectWithCORS(w, req, loginURL)
}

func (s *SSOWrapper) redirectWithCORS(w http.ResponseWriter, req *http.Request, uri string) {
	if s.EnableCORS {
		w.Header().Set("Access-Control-Allow-Origin", "*")
	}
	http.Redirect(w, req, uri, http.StatusFound)
}

// Extract the URL path from the service specification. The result
// will have both a leading and a trailing slash.
func pathFromService(service string) string {
	i := strings.IndexRune(service, '/')
	if i < 0 {
		return ""
	}
	return service[i:]
}

// Return a full URL from a HTTP request, assuming the given scheme
// (the URL field in net/http.Request normally only contains path and
// query args).
func getFullURL(req *http.Request, scheme string) *url.URL {
	u := *req.URL
	u.Scheme = scheme
	u.Host = req.Host
	return &u
}

func makeUniqueNonce() string {
	var b [8]byte
	if _, err := io.ReadFull(rand.Reader, b[:]); err != nil {
		panic(err)
	}
	return hex.EncodeToString(b[:])
}

// Return the origin from a URL (stripping path and other components).
func originFromURL(s string) string {
	parsed, err := url.Parse(s)
	if err != nil {
		return ""
	}
	return fmt.Sprintf("%s://%s", parsed.Scheme, parsed.Host)
}
