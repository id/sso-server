package client

import (
	"context"
	"errors"

	"git.autistici.org/ai3/go-common/clientutil"

	"git.autistici.org/id/keystore"
)

// ErrNoKeys indicates that the user has no encryption keys.
var ErrNoKeys = errors.New("no keys available")

// Client for the keystore API.
type Client interface {
	Open(context.Context, string, string, string, string, int) error
	Get(context.Context, string, string, string) ([]byte, error)
	Close(context.Context, string, string, string) error
}

type ksClient struct {
	be clientutil.Backend
}

// New returns a new Client with the given backend Config.
func New(config *clientutil.BackendConfig) (Client, error) {
	be, err := clientutil.NewBackend(config)
	if err != nil {
		return nil, err
	}
	return &ksClient{be}, nil
}

func (c *ksClient) Open(ctx context.Context, shard, username, password, sessionID string, ttl int) error {
	req := keystore.OpenRequest{
		Username:  username,
		Password:  password,
		TTL:       ttl,
		SessionID: sessionID,
	}
	var resp keystore.OpenResponse
	return c.be.Call(ctx, shard, "/api/open", &req, &resp)
}

func (c *ksClient) Get(ctx context.Context, shard, username, ssoTicket string) ([]byte, error) {
	req := keystore.GetRequest{
		Username:  username,
		SSOTicket: ssoTicket,
	}
	var resp keystore.GetResponse
	err := c.be.Call(ctx, shard, "/api/get_key", &req, &resp)
	if err != nil {
		return nil, err
	}
	if !resp.HasKey {
		return nil, ErrNoKeys
	}
	return resp.Key, err
}

func (c *ksClient) Close(ctx context.Context, shard, username, sessionID string) error {
	req := keystore.CloseRequest{
		Username:  username,
		SessionID: sessionID,
	}
	var resp keystore.CloseResponse
	return c.be.Call(ctx, shard, "/api/close", &req, &resp)
}
