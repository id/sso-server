package keystore

type OpenRequest struct {
	Username  string `json:"username"`
	Password  string `json:"password"`
	TTL       int    `json:"ttl"`
	SessionID string `json:"session_id"`
}

type OpenResponse struct{}

type GetRequest struct {
	Username  string `json:"username"`
	SSOTicket string `json:"sso_ticket"`
}

type GetResponse struct {
	HasKey bool   `json:"has_key"`
	Key    []byte `json:"key"`
}

type CloseRequest struct {
	Username  string `json:"username"`
	SessionID string `json:"session_id"`
}

type CloseResponse struct{}
