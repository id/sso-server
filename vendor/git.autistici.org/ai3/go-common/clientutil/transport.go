package clientutil

import (
	"crypto/tls"
	"net/http"
	"sync"
	"time"

	"git.autistici.org/ai3/go-common/tracing"
)

var defaultConnectTimeout = 30 * time.Second

// The transportCache is just a cache of http transports, each
// connecting to a specific address.
//
// We use this to control the HTTP Host header and the TLS ServerName
// independently of the target address.
type transportCache struct {
	tlsConfig      *tls.Config
	connectTimeout time.Duration

	mx         sync.RWMutex
	transports map[string]http.RoundTripper
}

func newTransportCache(tlsConfig *tls.Config, connectTimeout time.Duration) *transportCache {
	if connectTimeout == 0 {
		connectTimeout = defaultConnectTimeout
	}
	return &transportCache{
		tlsConfig:      tlsConfig,
		connectTimeout: connectTimeout,
		transports:     make(map[string]http.RoundTripper),
	}
}

func (m *transportCache) newTransport(addr string) http.RoundTripper {
	return tracing.WrapTransport(&http.Transport{
		TLSClientConfig: m.tlsConfig,
		DialContext:     netDialContext(addr, m.connectTimeout),

		// Parameters match those of net/http.DefaultTransport.
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	})
}

func (m *transportCache) getTransport(addr string) http.RoundTripper {
	m.mx.RLock()
	t, ok := m.transports[addr]
	m.mx.RUnlock()

	if !ok {
		m.mx.Lock()
		if t, ok = m.transports[addr]; !ok {
			t = m.newTransport(addr)
			m.transports[addr] = t
		}
		m.mx.Unlock()
	}

	return t
}
