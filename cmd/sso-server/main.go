package main

import (
	"flag"
	"io/ioutil"
	"log"

	"git.autistici.org/ai3/go-common/serverutil"
	"git.autistici.org/id/auth/client"
	"gopkg.in/yaml.v3"

	"git.autistici.org/id/sso-server/server"
)

var (
	addr       = flag.String("addr", ":4141", "tcp `address` to listen on")
	configFile = flag.String("config", "/etc/sso/server.yml", "configuration `file`")
	authSocket = flag.String("auth-socket", client.DefaultSocketPath, "authentication socket `path`")
)

// Config wraps together the sso-server configuration and the standard
// HTTP server config.
type Config struct {
	server.Config `yaml:",inline"`
	ServerConfig  *serverutil.ServerConfig `yaml:"http_server"`
}

func loadConfig(path string) (*Config, error) {
	// Read YAML config.
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	var config Config
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}
	return &config, nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	config, err := loadConfig(*configFile)
	if err != nil {
		log.Fatal(err)
	}
	if err = config.Config.Compile(); err != nil {
		log.Fatal(err)
	}

	loginService, err := server.NewLoginService(&config.Config)
	if err != nil {
		log.Fatal(err)
	}

	authClient := client.New(*authSocket)
	h, err := server.New(loginService, authClient, &config.Config)
	if err != nil {
		log.Fatal(err)
	}

	if err := serverutil.Serve(h, config.ServerConfig, *addr); err != nil {
		log.Fatal(err)
	}
}
