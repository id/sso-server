package saml

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/crewjam/saml"
	"github.com/crewjam/saml/logger"
	"github.com/gorilla/mux"
	"gopkg.in/yaml.v3"

	"git.autistici.org/id/go-sso/httpsso"
	"git.autistici.org/id/sso-server/common"
)

// Lifetime of an authenticated session.
var samlAuthTTL = 1 * time.Hour

type serviceProvider struct {
	// Descriptor can either be an inline XML document, or it can
	// be read from a file with the syntax "@filename".
	Descriptor string   `yaml:"descriptor"`
	SSOGroups  []string `yaml:"sso_groups"`

	parsed *saml.EntityDescriptor
}

type Config struct {
	BaseURL string `yaml:"base_url"`

	UsersFile      string `yaml:"users_file"`
	EmailUsernames bool   `yaml:"email_usernames"`

	// SAML X509 credentials.
	CertificateFile string `yaml:"certificate_file"`
	PrivateKeyFile  string `yaml:"private_key_file"`

	// SSO configuration.
	SessionAuthKey    common.SessionAuthenticationKey `yaml:"session_auth_key"`
	SessionEncKey     common.SessionEncryptionKey     `yaml:"session_enc_key"`
	SSOLoginServerURL string                          `yaml:"sso_server_url"`
	SSOPublicKeyFile  string                          `yaml:"sso_public_key_file"`
	SSODomain         string                          `yaml:"sso_domain"`

	// Service provider config.
	ServiceProviders   []*serviceProvider `yaml:"service_providers"`
	serviceProviderMap map[string]*serviceProvider
}

// Sanity checks for the configuration.
func (c *Config) check() error {
	if len(c.SessionAuthKey) == 0 {
		return errors.New("session_auth_key is empty")
	}
	if len(c.SessionEncKey) == 0 {
		return errors.New("session_enc_key is empty")
	}
	if c.SSOLoginServerURL == "" {
		return errors.New("sso_server_url is empty")
	}
	if c.SSODomain == "" {
		return errors.New("sso_domain is empty")
	}
	return nil
}

func (c *Config) loadServiceProviders() error {
	c.serviceProviderMap = make(map[string]*serviceProvider)
	for _, sp := range c.ServiceProviders {
		var data []byte
		if strings.HasPrefix(sp.Descriptor, "@") {
			var err error
			data, err = ioutil.ReadFile(sp.Descriptor[1:])
			if err != nil {
				return fmt.Errorf("reading %s: %w", sp.Descriptor[1:], err)
			}
		} else {
			data = []byte(sp.Descriptor)
		}
		var ent saml.EntityDescriptor
		if err := xml.Unmarshal(data, &ent); err != nil {
			return fmt.Errorf("error unmarshaling descriptor: %w", err)
		}
		sp.parsed = &ent
		c.serviceProviderMap[ent.EntityID] = sp
	}
	return nil
}

func (c *Config) GetServiceProvider(r *http.Request, serviceProviderID string) (*saml.EntityDescriptor, error) {
	sp, ok := c.serviceProviderMap[serviceProviderID]
	if !ok {
		return nil, os.ErrNotExist
	}
	return sp.parsed, nil
}

func (c *Config) GetSSOGroups(serviceProviderID string) []string {
	sp, ok := c.serviceProviderMap[serviceProviderID]
	if !ok {
		return nil
	}
	return sp.SSOGroups
}

func (c *Config) GetAllSSOGroups() []string {
	tmp := make(map[string]struct{})
	for _, sp := range c.serviceProviderMap {
		for _, group := range sp.SSOGroups {
			tmp[group] = struct{}{}
		}
	}
	var out []string
	for group := range tmp {
		out = append(out, group)
	}
	return out
}

// Read users from a YAML-encoded file, in a format surprisingly
// compatible with git.autistici.org/id/auth/server.
//
// TODO: Make it retrieve the email addresses as extra data in the SSO
// token (this feature is currently unsupported by the SSO server,
// even though the auth-server provides the information).
type userInfo struct {
	Name  string `yaml:"name"`
	Email string `yaml:"email"`
}

type userBackend interface {
	GetUser(string) (*userInfo, bool)
}

type userFileBackend struct {
	users map[string]*userInfo
}

func (b *userFileBackend) GetUser(username string) (*userInfo, bool) {
	info, ok := b.users[username]
	return info, ok
}

func newUserFileBackend(path string) (*userFileBackend, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	var userList []*userInfo
	if err := yaml.Unmarshal(data, &userList); err != nil {
		return nil, err
	}
	users := make(map[string]*userInfo)
	for _, u := range userList {
		users[u.Name] = u
	}
	return &userFileBackend{
		users: users,
	}, nil
}

type identityUserBackend struct{}

func (b *identityUserBackend) GetUser(username string) (*userInfo, bool) {
	return &userInfo{Name: username, Email: username}, true
}

type sessionProvider struct {
	config *Config
	users  userBackend
}

func newSessionProvider(config *Config) (*sessionProvider, error) {
	var users userBackend
	var err error
	switch {
	case config.UsersFile != "":
		users, err = newUserFileBackend(config.UsersFile)
		if err != nil {
			return nil, err
		}
	case config.EmailUsernames:
		users = new(identityUserBackend)
	default:
		return nil, errors.New("neither users_file or email_usernames are specified")
	}
	return &sessionProvider{
		config: config,
		users:  users,
	}, nil
}

// Nice little O(N^2) algorithm right there...
func matchGroups(user, exp []string) bool {
	if exp == nil {
		return true
	}
	for _, ug := range user {
		for _, eg := range exp {
			if ug == eg {
				return true
			}
		}
	}
	return false
}

func (sp *sessionProvider) GetSession(w http.ResponseWriter, r *http.Request, req *saml.IdpAuthnRequest) *saml.Session {
	// Check for authentication by verifying the SSO username. We
	// also need to be able to retrieve user information from the
	// backend, to match SSO. Group membership, if enabled in our
	// configuration, is also verified at this stage.
	username := httpsso.Username(r)
	if username == "" {
		log.Printf("error: could not retrieve user from SSO")
		http.Error(w, "No user found", http.StatusInternalServerError)
		return nil
	}

	if !matchGroups(httpsso.Groups(r), sp.config.GetSSOGroups(req.ServiceProviderMetadata.ID)) {
		http.Error(w, "Forbidden (bad group)", http.StatusForbidden)
		return nil
	}

	user, ok := sp.users.GetUser(username)
	if !ok {
		log.Printf("error: user %s is authenticated but unknown", username)
		http.Error(w, "User not found", http.StatusInternalServerError)
		return nil
	}

	log.Printf("successfully authenticated session for username=%s, provider=%s", username, req.ServiceProviderMetadata.ID)

	return &saml.Session{
		ID:             base64.StdEncoding.EncodeToString(common.RandomBytes(32)),
		CreateTime:     saml.TimeNow(),
		ExpireTime:     saml.TimeNow().Add(sessionMaxAge),
		Index:          hex.EncodeToString(common.RandomBytes(32)),
		NameID:         user.Name,
		UserName:       user.Name,
		UserEmail:      user.Email,
		UserCommonName: user.Name,
		UserGivenName:  user.Name,
	}
}

func NewSAMLIDP(config *Config) (http.Handler, error) {
	if err := config.check(); err != nil {
		return nil, err
	}
	if err := config.loadServiceProviders(); err != nil {
		return nil, err
	}

	tlsCert, err := tls.LoadX509KeyPair(config.CertificateFile, config.PrivateKeyFile)
	if err != nil {
		return nil, err
	}
	x509Cert, err := x509.ParseCertificate(tlsCert.Certificate[0])
	if err != nil {
		return nil, err
	}

	pkey, err := ioutil.ReadFile(config.SSOPublicKeyFile)
	if err != nil {
		return nil, err
	}

	w, err := httpsso.NewSSOWrapper(config.SSOLoginServerURL, pkey, config.SSODomain, []byte(config.SessionAuthKey), []byte(config.SessionEncKey), samlAuthTTL)
	if err != nil {
		return nil, err
	}

	baseURL, err := url.Parse(config.BaseURL)
	if err != nil {
		return nil, err
	}
	ssoURL := *baseURL
	ssoURL.Path += "/login/"
	metadataURL := *baseURL
	metadataURL.Path += "/metadata"
	svc := fmt.Sprintf("%s%s", ssoURL.Host, ssoURL.Path)
	if !strings.HasSuffix(svc, "/") {
		svc += "/"
	}

	logoutURL, err := url.Parse(
		strings.TrimRight(config.SSOLoginServerURL, "/") + "/logout")
	if err != nil {
		return nil, err
	}

	sp, err := newSessionProvider(config)
	if err != nil {
		return nil, err
	}

	// Create the SAML IdentityProvider, but then we put another
	// mux.Router in front in order to wrap just the ssoURL with
	// our own SSO handler.
	idp := &saml.IdentityProvider{
		Key:                     tlsCert.PrivateKey,
		Certificate:             x509Cert,
		Logger:                  logger.DefaultLogger,
		MetadataURL:             metadataURL,
		SSOURL:                  ssoURL,
		LogoutURL:               *logoutURL,
		ServiceProviderProvider: config,
		SessionProvider:         sp,
	}
	h := idp.Handler()

	root := mux.NewRouter()
	root.PathPrefix(ssoURL.Path).Handler(w.Wrap(h, svc, config.GetAllSSOGroups()))
	root.Handle(metadataURL.Path, h)
	return root, nil
}

var sessionMaxAge = 300 * time.Second
